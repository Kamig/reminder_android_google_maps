package pl.edu.pw.mini.grastudio.reminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;

/**
 * Created by Kamil on 15.04.2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper implements IDatabaseHandler {
    //All Static variables
    //Database Version
    private static final int DATABASE_VERSION = 1;

    //Database Name
    private static final String DATABASE_NAME = "locationListManager";

    //table name
    private static final String TABLE_LOCATIONS = "locationList";
    private static final String TABLE_EVENTS_LOCATIONS = "eventsLocationsList";
    private static final String TABLE_EVENTS = "eventsList";


    //common columns names
    private static final String KEY_ID = "id";

    //locationsTable Columns names
    private static final String KEY_NAME = "name";
    private  static  final String KEY_LAT = "lat";
    private  static  final  String KEY_LNG = "lng";
    private static final String KEY_FAVORITE_FLAG = "favorite_flag";

    //eventLocationsTable Columns names
    private static final String KEY_EVENT_ID = "event_id";
    private static final String KEY_LOCATION_ID = "location_id";

    //eventsTable Column names
    private static final String KEY_DURATION = "duration";
    private static final String KEY_PRIORITY = "priority";
    private static final String KEY_TRANSPORT = "transport";
    private static final String KEY_DISTANCE = "distance";


    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_SHOPPING_TABLE = "CREATE TABLE " + TABLE_LOCATIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_LAT + " REAL," + KEY_LNG + " REAL,"
                + KEY_FAVORITE_FLAG + " INTEGER DEFAULT 0" +")";
        db.execSQL(CREATE_SHOPPING_TABLE);

        String CREATE_EVENTS_LOCATIONS_TABLE = "CREATE TABLE " + TABLE_EVENTS_LOCATIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_EVENT_ID + " INTEGER," + KEY_LOCATION_ID + " INTEGER"
                 + ")";
        db.execSQL(CREATE_EVENTS_LOCATIONS_TABLE);

        String CREATE_EVENTS_TABLE = "CREATE TABLE " + TABLE_EVENTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DURATION + " TEXT," + KEY_PRIORITY + " INTEGER,"
                + KEY_TRANSPORT + " INTEGER," + KEY_DISTANCE + " REAL" + ")";
        db.execSQL(CREATE_EVENTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);

        //Create tables again
        onCreate(db);
    }

    //Adding new location
    @Override
    public long addLocation(CustomerLocation location){
        //we never want to update locations, so if it has an id than do nothing
        if(location.getId() != -1)
            return location.getId();
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();

        ContentValues values =  new ContentValues();
        values.put(KEY_NAME, location.getName()); //item name
        values.put(KEY_LAT, location.getLat()); //item name
        values.put(KEY_LNG, location.getLng()); //item name
        if(location.isFavorite()){
            values.put(KEY_FAVORITE_FLAG, 1);
        }
//        else {
//            values.put(KEY_FAVORITE_FLAG, 0);
//        }

        //Inserting Row
        long id = db.insert(TABLE_LOCATIONS, null, values);
         //Closing database connection
        return id;
    }

    @Override
    public long addEventLocation(EventLocationLink link){
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_ID, link.getEventId());
        values.put(KEY_LOCATION_ID, link.getLocationId());

        long id = db.insert(TABLE_EVENTS_LOCATIONS, null, values);
        Log.d("INFO", String.valueOf(id));
        

        return id;
    }

    @Override
    public long updateEventLocation(EventLocationLink link) {
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_ID, link.getEventId());
        values.put(KEY_LOCATION_ID, link.getLocationId());

        long id = db.update(TABLE_EVENTS_LOCATIONS, values, KEY_ID + " = ?", new String[]{String.valueOf(link.getId())});
        Log.d("INFO", String.valueOf(id));
        

        return id;
    }

    //Getting single location
    @Override
    public CustomerLocation getLocation(Long id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LOCATIONS, new String[]{KEY_ID, KEY_NAME, KEY_LAT, KEY_LNG, KEY_FAVORITE_FLAG}, KEY_ID + "=?", new String[] {id.toString()},
                null, null, null, null);

        if(cursor !=  null)
            cursor.moveToFirst();

       CustomerLocation location = new CustomerLocation(cursor.getLong(0), cursor.getString(1),
               cursor.getDouble(2), cursor.getDouble(3), cursor.getInt(4)== 1 );
        Log.d("INFO",
                String.valueOf(cursor.getLong(0))+ cursor.getString(1)+ String.valueOf(cursor.getDouble(2)) + String.valueOf(cursor.getDouble(3)) + String.valueOf(cursor.getDouble(4)) );

        cursor.close();
        

        return  location;
    }

    @Override
    public EventLocationLink getEventLocationFromId(Long id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS_LOCATIONS, new String[]{KEY_EVENT_ID, KEY_LOCATION_ID}, KEY_ID + "=?", new String[] {id.toString()},
                null, null, null, null);

        if(cursor !=  null)
            cursor.moveToFirst();
        if(cursor.getCount() == 0)
            return null;

        EventLocationLink eventLocationLink = new EventLocationLink(id, cursor.getLong(0), cursor.getLong(1));

        

        return  eventLocationLink;
    }

//    public EventLocationLink getEventLocationFromLocationId(Long locationId){
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_EVENTS_LOCATIONS, new String[]{KEY_EVENT_ID, KEY_LOCATION_ID}, KEY_LOCATION_ID + "=?", new String[] {locationId.toString()},
//                null, null, null, null);
//
//        if(cursor !=  null)
//            cursor.moveToFirst();
//
//        EventLocationLink eventLocationLink = new EventLocationLink(cursor.getLong(0), cursor.getLong(1));
//
//        return  eventLocationLink;
//    }

    @Override
    public EventLocationLink getEventLocationFromEventId(Long eventId){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS_LOCATIONS, new String[]{KEY_ID, KEY_EVENT_ID, KEY_LOCATION_ID}, KEY_EVENT_ID + "=?", new String[] {eventId.toString()},
                null, null, null, null);

        if(cursor !=  null)
            cursor.moveToFirst();
        else
            return null;
        if(cursor.getCount() == 0) {
            cursor.close();
            
            return null;
        }

        EventLocationLink eventLocationLink = new EventLocationLink(cursor.getLong(0), cursor.getLong(1), cursor.getLong(2));

        cursor.close();
        

        return  eventLocationLink;
    }

    @Override
    public List<EventLocationLink> getEventLocationFromLocationId(Long locationId){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS_LOCATIONS, new String[]{KEY_ID, KEY_EVENT_ID, KEY_LOCATION_ID}, KEY_LOCATION_ID + "=?", new String[] {locationId.toString()},
                null, null, null, null);

        List<EventLocationLink> result = new ArrayList<>();

        while(cursor.moveToNext()) {
            result.add(new EventLocationLink(cursor.getLong(0), cursor.getLong(1), cursor.getLong(2)));
        }

        cursor.close();
        

        return result;
    }

    //Getting all locations
    @Override
    public List<CustomerLocation> getAllLocations(){

        List<CustomerLocation> locationList = new ArrayList<CustomerLocation>() ;
        //Select All Query
        String selctQuery = "SELECT * FROM " + TABLE_LOCATIONS;

        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();
        Cursor cursor = db.rawQuery(selctQuery, null);

        //looping through all rows and adding to list
        if(cursor.moveToFirst()){
            do{
                CustomerLocation location = new CustomerLocation(cursor.getLong(0), cursor.getString(1),
                        Double.parseDouble(cursor.getString(2)), Double.parseDouble(cursor.getString(3)), cursor.getInt(4)== 1 );

                //Adding shoppingItem to list
                locationList.add(location);
            }while (cursor.moveToNext());
        }

        cursor.close();
        

        return locationList;
    }

    @Override
    public List<EventLocationLink> getAllEventLocationLinks(){

        List<EventLocationLink> eventLocationLinks = new ArrayList<EventLocationLink>() ;
        //Select All Query
        String selctQuery = "SELECT * FROM " + TABLE_EVENTS_LOCATIONS;

        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();
        Cursor cursor = db.rawQuery(selctQuery, null);

        //looping through all rows and adding to list
        if(cursor.moveToFirst()){
            do{
                EventLocationLink eventLocationLink= new EventLocationLink(cursor.getLong(0), cursor.getLong(1), cursor.getLong(2));
                Log.d("INFO", String.valueOf(cursor.getLong(0)) + String.valueOf(cursor.getLong(1)) + String.valueOf(cursor.getLong(2)));
                //Adding shoppingItem to list
                eventLocationLinks.add(eventLocationLink);
            }while (cursor.moveToNext());
        }

        cursor.close();
        

        return eventLocationLinks;
    }

    //Getting location count
    @Override
    public int getLocationCount(){
        String countQuery = "SELECT * FROM " + TABLE_LOCATIONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        

        //return count
        return cursor.getCount();
    }

    //Updating single location
    @Override
    public long updateLocation(CustomerLocation location){
        SQLiteDatabase db =  DatabaseManager.getInstance(this).openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, location.getName());
        values.put(KEY_LAT, location.getLat());
        values.put(KEY_LNG, location.getLng());
        values.put(KEY_FAVORITE_FLAG, location.isFavorite());

        //updating row
        long result =  db.update(TABLE_LOCATIONS, values, KEY_ID + " = ?", new String[]{String.valueOf(location.getId())});

        
        return location.getId();
    }

    //Deleting single shopping item
    @Override
    public int deleteLocation(long id){
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();
        int ret = db.delete(TABLE_LOCATIONS, KEY_ID + " = ?", new String[]{String.valueOf(id)});
        
        return ret;
    }

    @Override
    public int deleteEventLocationLink(long id){
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();
        int ret = db.delete(TABLE_EVENTS_LOCATIONS, KEY_ID + " = ?", new String[]{String.valueOf(id)});
        
        return ret;
    }

    @Override
    public void addEvent(Event event) {
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, event.getId());
        if(event.isAllDay())
            values.put(KEY_DURATION, event.getRFC2445Duration());
        values.put(KEY_PRIORITY, event.getPriorityInt());
        values.put(KEY_DISTANCE, event.getReminderDistanceMeters());
        values.put(KEY_TRANSPORT, event.getTransportInt());

        db.insert(TABLE_EVENTS, null, values);
        
    }

    @Override
    public void updateEvent(Event event) {
        SQLiteDatabase db = DatabaseManager.getInstance(this).openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, event.getId());
        if(event.isAllDay())
            values.put(KEY_DURATION, event.getRFC2445Duration());
        values.put(KEY_PRIORITY, event.getPriorityInt());
        values.put(KEY_TRANSPORT, event.getTransportInt());
        values.put(KEY_DISTANCE, event.getReminderDistanceMeters());


        db.update(TABLE_EVENTS, values, KEY_ID + " = ?", new String[]{String.valueOf(event.getId())});
        
    }

    @Override
    public void getEvent(Event event) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS, new String[]{KEY_ID, KEY_DURATION, KEY_PRIORITY, KEY_TRANSPORT, KEY_DISTANCE}, KEY_ID + "=?"
                , new String[] {Long.toString(event.getId())},
                null, null, null, null);

        try {
            if (cursor == null || !cursor.moveToFirst())
                return;

            event.setRFC2445Duration(cursor.getString(1));
            event.setPriority(cursor.getInt(2));
            event.setMeanOfTransport(cursor.getInt(3));
            event.setReminderDistanceMeters(cursor.getDouble(4));
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
            
        }

    }

}//class
