package pl.edu.pw.mini.grastudio.reminder;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.GetContenerForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetNotAllDayEventsNearNowTimeUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTheNearestEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTodayAllDayEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetContenerForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IUseCase;
import pl.edu.pw.mini.grastudio.reminder.googleApi.ILocationHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.IRouteHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.LocationHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.RouteHelper;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationContener;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.DateService;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static java.lang.System.in;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ReminderService extends IntentService implements IMessage{

    Location mLastLocation;
    Event currentEvent;
    List<Event> eventList;
    List<Event> nearEventList;
    CustomerLocation mLastEventLocation;
     IRouteHelper routeHelper;

    public ReminderService() {
        super("ReminderService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        eventList = new ArrayList<Event>();
        nearEventList = new ArrayList<Event>();
        routeHelper = new RouteHelper(this);
        Log.d("INFO","Service");
        IUseCase<Event> getTheNearestEventUseCase
                = new GetTheNearestEventUseCase(new EventRepository(this, new DatabaseHandler(this)), new DateService());
         IGetLocationForEventUseCase getLocationForEventUseCase  = new GetLocationForEventUseCase(
                new LocationsRepository(new DatabaseHandler(this)), new EventsLocationsRepository(new DatabaseHandler(this)));
        ILocationHelper locationHelper = new LocationHelper(this, null);

        IUseCase<List<Event>> getTodayAllDayEventsUseCase
                = new GetTodayAllDayEventsUseCase(new EventRepository(this, new DatabaseHandler(this)), new DateService());

        IUseCase<List<Event>> getNotAllDayEventsNearNowTimeUseCase
                = new GetNotAllDayEventsNearNowTimeUseCase(new EventRepository(this, new DatabaseHandler(this)), new DateService());

        IGetContenerForEventUseCase getContenerForEventUseCase  = new GetContenerForEventUseCase(
                new LocationsRepository(new DatabaseHandler(this)), new EventsLocationsRepository(new DatabaseHandler(this)));

        int i = 0;
        while(i < 1000){
            try {
                Log.d("INFO","While");
                Thread.sleep(10000);
                mLastLocation = locationHelper.getCurrentLocation();
                if (mLastLocation != null) {
                    checkTodayNearInDistanceEvents(getContenerForEventUseCase, getTodayAllDayEventsUseCase);
                    checkTodayNearInDistanceEvents(getContenerForEventUseCase, getNotAllDayEventsNearNowTimeUseCase);
                    checkNearestEvent(getLocationForEventUseCase, getTheNearestEventUseCase);
                }
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkNearestEvent(final IGetLocationForEventUseCase getLocationForEventUseCase, IUseCase<Event> getTheNearestEventUseCase){
        getTheNearestEventUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, Event>() {
                    @Override
                    public Event call(Throwable throwable) {
                        //there was an error while getting events
                        throwable.printStackTrace();
                        Log.d("INFO", "nearest event error");
                        return null;
                    }
                })
                .subscribe(new Action1<Event>() {
                    @Override
                    public void call(final Event event) {
                        // locations.add(location);
                        if(event == null) return;
                        currentEvent = event;
                        getLocationForEventUseCase.setEvent(event);
                        getLocationForEventUseCase.execute()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                                    @Override
                                    public CustomerLocation call(Throwable throwable) {
                                        //there was an error while getting events
                                        throwable.printStackTrace();
                                        Log.d("INFO", "location from event");
                                        return null;
                                    }
                                })
                                .subscribe(new Action1<CustomerLocation>() {
                                    @Override
                                    public void call(CustomerLocation location) {
                                        if(location != null) {
                                            mLastEventLocation = location;
                                            routeHelper.TimeFromPointToPoint(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()),
                                                    new LatLng(location.getLat(), location.getLng()), TimeDifference(event.getStartDate()), ReminderService.this, event.getTransportInt()) ;
                                            Log.d("INFO", location.getName() + " " + event.getName());
                                        }
                                    }
                                });
                    }
                });
    }

    private void checkTodayNearInDistanceEvents(final IGetContenerForEventUseCase getContenerForEventUseCase, IUseCase<List<Event>> getTodayAllDayEventsUseCase){
        getTodayAllDayEventsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        //there was an error while getting events
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(final List<Event> events) {
                        if(events != null) {
                           for(int i = 0; i < events.size(); i++){
                               getContenerForEventUseCase.setEvent(events.get(i));
                               getContenerForEventUseCase.execute()
                                       .subscribeOn(Schedulers.io())
                                       .observeOn(AndroidSchedulers.mainThread())
                                       .onErrorReturn(new Func1<Throwable, EventLocationContener>() {
                                           @Override
                                           public EventLocationContener call(Throwable throwable) {
                                               //there was an error while getting events
                                               throwable.printStackTrace();
                                               Log.d("INFO", "location from event");
                                               return null;
                                           }
                                       })
                                       .subscribe(new Action1<EventLocationContener>() {
                                           @Override
                                           public void call(EventLocationContener contener) {
                                               if(contener!= null) {
                                                    if( contener.getEvent().getReminderDistanceMeters() >= routeHelper.DistanceFromPointToPoint(
                                                            new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()),
                                                            new LatLng(contener.getLocation().getLat(), contener.getLocation().getLng()))){
                                                        if(isNew(nearEventList, contener.getEvent())) {
                                                            nearEventList.add(contener.getEvent());
                                                            Intent resultIntent = new Intent(ReminderService.this, TabActivity.class);
                                                            showNotification(contener.getEvent().getName(), "Jesteś na miejscu!", resultIntent);
                                                        }
                                                    }
                                               }
                                           }
                                       });
                           }
                        }
                    }
                });
    }





    @Override
    public void ShowMessage() {
        Log.d("INFO", "Musisz już wyjść, żeby zdążyć!");
        if(isNew(eventList,currentEvent) && isNew(nearEventList, currentEvent)) {
            eventList.add(currentEvent);
            Intent resultIntent = new Intent(this, RouteActivity.class);
            resultIntent.putExtra(getResources().getString(R.string.extra_title), currentEvent.getName());
            resultIntent.putExtra(getResources().getString(R.string.traveling_mode), currentEvent.getTransportInt());
            showNotification(currentEvent.getName(), "Musisz wyjść, żeby zdążyć!", resultIntent);
        }

    }

    private void showNotification(String title, String contnent, Intent resultIntent){
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_add_white_24dp)
                        .setContentTitle(title)
                        .setContentText(contnent)
                        .setSound(alarmSound);
        // Creates an explicit intent for an Activity in your app
        if(mLastEventLocation != null) {
            resultIntent.putExtra(getResources().getString(R.string.extra_lat), mLastEventLocation.getLat());
            resultIntent.putExtra(getResources().getString(R.string.extra_lng), mLastEventLocation.getLng());
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
    }

    private int TimeDifference(Date date){
        Calendar today = Calendar.getInstance();
        today.setTime(Calendar.getInstance().getTime());
        
        long diffInMs = date.getTime() - today.getTime().getTime();
        Log.d("INFO", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(diffInMs)));

        return   (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs);
    }

    private boolean isNew(List<Event> list, Event event){
        for(Event e: list){
            if(e.getId() == event.getId())
                return false;
        }
        return true;
    }
}
