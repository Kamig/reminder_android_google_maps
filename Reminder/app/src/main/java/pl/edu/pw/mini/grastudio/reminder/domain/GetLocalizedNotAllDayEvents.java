package pl.edu.pw.mini.grastudio.reminder.domain;

import android.util.Log;
import android.util.Pair;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.googleApi.IRouteHelper;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.model.LocalizedEvent;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;

public class GetLocalizedNotAllDayEvents implements IGetLocalizedNotAllDayEvents {

    private Date date;
    private LatLng firstLocation;
    private IRepository<Event> eventRepository;
    private IEventsLocationsRepository eventsLocationsRepository;
    private IRepository<CustomerLocation> locationsRepository;
    private IRouteHelper routeHelper;

    public GetLocalizedNotAllDayEvents(IRepository<Event> eventRepository,
                                       IEventsLocationsRepository eventsLocationsRepository,
                                       IRepository<CustomerLocation> locationsRepository,
                                       IRouteHelper routeHelper) {
        this.eventRepository = eventRepository;
        this.eventsLocationsRepository = eventsLocationsRepository;
        this.locationsRepository = locationsRepository;
        this.routeHelper = routeHelper;
    }


    @Override
    public Observable<List<LocalizedEvent>> execute() {
        return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            @Override
            public Boolean call(Event event) {
                return sameDate(date,event.getStartDate()) && !event.isAllDay();
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        }).flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).flatMap(new Func1<Event, Observable<LocalizedEvent>>() {
            @Override
            public Observable<LocalizedEvent> call(final Event event) {
                return eventsLocationsRepository.getByEventId(event.getId()).flatMap(new Func1<EventLocationLink, Observable<LocalizedEvent>>() {
                    @Override
                    public Observable<LocalizedEvent> call(EventLocationLink eventLocationLink) {
                        final LocalizedEvent result = new LocalizedEvent(event);
                        if(eventLocationLink == null)
                            return  Observable.just(result);
                        return locationsRepository.getById(eventLocationLink.getLocationId()).flatMap(new Func1<CustomerLocation, Observable<LocalizedEvent>>() {
                            @Override
                            public Observable<LocalizedEvent> call(CustomerLocation location) {
                                result.setLocation(location);
                                return Observable.just(result);
                            }
                        });
                    }
                });
            }
        })
        .filter(new Func1<LocalizedEvent, Boolean>() { //only events with location
            @Override
            public Boolean call(LocalizedEvent localizedEvent) {
                return localizedEvent.getLocation() != null;
            }
        })
        .scan(getEventPair(null,new LocalizedEvent(firstLocation)),new Func2<Pair<LocalizedEvent, LocalizedEvent>,
                LocalizedEvent, Pair<LocalizedEvent, LocalizedEvent>>() {
            @Override
            public Pair<LocalizedEvent, LocalizedEvent> call(final Pair<LocalizedEvent, LocalizedEvent> from,
                                                             final LocalizedEvent to) {

                return getEventPair(from.second, to);
            }
        })
        .skip(1)
        .flatMap(new Func1<Pair<LocalizedEvent, LocalizedEvent>, Observable<LocalizedEvent>>() {
            @Override
            public Observable<LocalizedEvent> call(final Pair<LocalizedEvent, LocalizedEvent> pair) {
                return routeHelper.ObservableTimeFromPointToPoint(pair.first.getLocation().getLatLng(),
                        pair.second.getLocation().getLatLng(),
                        pair.second.getTransportInt())
                        .map(new Func1<Integer, LocalizedEvent>() {
                            @Override
                            public LocalizedEvent call(Integer travelTime) {
                                pair.second.setTravelTime(travelTime / 60);
                                return pair.second;
                            }
                        });
            }
        })
        .toSortedList(new Func2<LocalizedEvent, LocalizedEvent, Integer>() { //sort chronologically
            @Override
            public Integer call(LocalizedEvent event, LocalizedEvent event2) {
                return event.getTravelStartTime().compareTo(event2.getTravelStartTime());
            }
        });
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }
    @Override
    public void setFirstLocation(LatLng firstLocation) {
        this.firstLocation = firstLocation;
    }

    private Pair<LocalizedEvent, LocalizedEvent> getEventPair(LocalizedEvent a, LocalizedEvent b) {
        return new Pair<>(a, b);
    }

    private boolean sameDate(Date date1, Date date2){
        Calendar first = Calendar.getInstance();
        first.setTime(date1);

        Calendar second = Calendar.getInstance();
        second.setTime(date2);

        return first.get(Calendar.DAY_OF_YEAR) == second.get(Calendar.DAY_OF_YEAR) ;
    }
}
