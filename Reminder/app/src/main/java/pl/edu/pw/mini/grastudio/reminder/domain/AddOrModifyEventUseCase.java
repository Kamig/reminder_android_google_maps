package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import rx.Observable;

public class AddOrModifyEventUseCase implements IAddOrModifyEventUseCase {

    private EventRepository repository;
    private Event eventToAdd;

    public AddOrModifyEventUseCase(EventRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setEventToAdd(Event eventToAdd) {
        this.eventToAdd = eventToAdd;
    }

    @Override
    public Observable<Long> execute() {
        return repository.addOrUpdate(eventToAdd);
    }
}
