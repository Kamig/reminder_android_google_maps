package pl.edu.pw.mini.grastudio.reminder.domain;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.IDateService;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Kamil on 10.06.2017.
 */

public class GetNotAllDayEventsNearNowTimeUseCase implements IUseCase<List<Event>> {
    private IRepository<Event> eventRepository;
    private IDateService dateService;
    private int timeToEvent = 5;

    public GetNotAllDayEventsNearNowTimeUseCase (IRepository<Event> eventRepository, IDateService dateService) {
        this.eventRepository = eventRepository;
        this.dateService = dateService;
    }
    @Override
    public Observable<List<Event>> execute() {
        //we need to use flatMapIterable, because otherwise we cannot filter the list
        return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(Event event) {
                return  nearNow(dateService.Now(), event.getStartDate());
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        });
    }

    private boolean nearNow(Date date1, Date date2){

        long diffInMs = date2.getTime() - date1.getTime();
        Log.d("INFO", String.valueOf((int)TimeUnit.MINUTES.toMinutes(diffInMs)));
        return   (int)TimeUnit.MILLISECONDS.toMinutes(diffInMs) < timeToEvent && (int)TimeUnit.MILLISECONDS.toMinutes(diffInMs) >= 0;

    }
}
