package pl.edu.pw.mini.grastudio.reminder.utils;


import android.content.Context;
import android.content.res.Resources;

import pl.edu.pw.mini.grastudio.reminder.model.Event;

public class EnumUtils {
//    public static String ToLocalizedString(Event.Priority priority, Context context) {
//        Resources resources = context.getResources();
//        int id = resources.getIdentifier(priority.name(), "string", context.getPackageName());
//        if(id != 0) {
//            //the name was found in the resources
//            return resources.getString(id);
//        }
//        //otherwise just return the name
//        return priority.name();
//    }

    public static <T extends Enum<T>> String ToLocalizedString(Enum<T> e, Context context) {
        Resources resources = context.getResources();
        int id = resources.getIdentifier(e.name(), "string", context.getPackageName());
        if(id != 0) {
            //the name was found in the resources
            return resources.getString(id);
        }
        //otherwise just return the name
        return e.name();
    }
}
