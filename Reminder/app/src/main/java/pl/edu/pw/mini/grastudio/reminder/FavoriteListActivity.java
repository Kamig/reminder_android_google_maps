package pl.edu.pw.mini.grastudio.reminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.adapter.CustomerLocationsAdapter;
import pl.edu.pw.mini.grastudio.reminder.adapter.EventsAdapter;
import pl.edu.pw.mini.grastudio.reminder.adapter.ItemTouchHelperCallback;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.DeleteLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFavoriteLocationsUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.presenter.FavoriteLocationsPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IFavoriteLocationsPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.IFavoriteView;

public class FavoriteListActivity extends Fragment implements IFavoriteView, CustomerLocationsAdapter.OnLocationDeletedListener {

    private CustomerLocationsAdapter mAdapter;
    private IFavoriteLocationsPresenter favoriteLocationsPresenter;
    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;

    class RequestCode {
        static final int ADD_ACTIVITY = 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.content_favorite_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());
        mAdapter = new CustomerLocationsAdapter();
        initializePresenter();
        initializeRecyclerView();

        floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        //floatingActionButton.bringToFront();
    }

    @Override
    public void onStart() {
        super.onStart();
        favoriteLocationsPresenter.onStart();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser  && getActivity() != null) {
            if(floatingActionButton == null)
                floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
            floatingActionButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_add_white_24dp));
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    favoriteLocationsPresenter.chooseNewFavoriteLocation();
                }
            });
        }
    }

    @Override
    public void onDeleted(CustomerLocation location) {
        favoriteLocationsPresenter.onLocationDeleted(location);
    }

    @Override
    public void showLocations(List<CustomerLocation> customerLocations) {
    mAdapter.replaceData(customerLocations);
    }

    @Override
    public void startPlacePickerDialog() {
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//        try {
//            Intent intent = builder.build(this);
//            startActivityForResult(intent, MapsActivity.RequestCode.PICK_LOCATION);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
        Intent intent = new Intent(getActivity(), AddFavoriteActivity.class);
        startActivityForResult(intent, RequestCode.ADD_ACTIVITY);
    }

    @Override
    public void addToRecyclerView(CustomerLocation customerLocation) {
        mAdapter.addLocation(customerLocation);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.ADD_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                String serializedData = data.getStringExtra(AddFavoriteActivity.ARG_LOCATION);
                favoriteLocationsPresenter.onLocationAdded(serializedData);
            }
        }
    }

    private void initializeRecyclerView(){
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.attachListener(this);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
    private void initializePresenter(){
        favoriteLocationsPresenter = new FavoriteLocationsPresenter(new AddOrModifyLocationUseCase(new LocationsRepository(new DatabaseHandler(getActivity()))),
                new GetFavoriteLocationsUseCase(new LocationsRepository(new DatabaseHandler(getActivity()))),
                new AddOrModifyLocationUseCase(new LocationsRepository(new DatabaseHandler(getActivity()))));
        favoriteLocationsPresenter.attachView(this);
    }
}
