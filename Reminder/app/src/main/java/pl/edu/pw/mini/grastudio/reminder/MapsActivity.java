package pl.edu.pw.mini.grastudio.reminder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.R;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetEventForLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFilteredEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFutureEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetTodayEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.googleApi.RouteHelper;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.presenter.EventsPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.ILocationsPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.LocationsPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.DateService;
import pl.edu.pw.mini.grastudio.reminder.view.ILocationsView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MapsActivity extends android.support.v4.app.Fragment implements ILocationsView, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, TimeFilterFragment.TimeFilterListener
{
    private ILocationsPresenter locationsPresenter;
    private FloatingActionButton floatingActionButton;

    @Override
    public void SetDates(Date startDate, Date endDate) {
        locationsPresenter.filterEvents(startDate, endDate);
    }

    class RequestCode {
        static final int PICK_LOCATION = 1;
    }

    //Map
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_map, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content_map, mapFragment);
        fragmentTransaction.commit();

        mapFragment.getMapAsync(this);
        initializePresenter();

        floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
    }

    @Override
    public void onStart() {
        super.onStart();

        locationsPresenter.onStart();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser  && getActivity() != null) {
            if(floatingActionButton == null)
                floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
            floatingActionButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_today_white_24dp));
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    locationsPresenter.OnlyTodayEvents();
                }
            });
            locationsPresenter.updateMarkers();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        locationsPresenter.onStop();
    }

    private void initializePresenter() {
        //TODO: maybe use injection when I finally understand it
        locationsPresenter = new LocationsPresenter(new GetLocationForEventUseCase(new LocationsRepository(new DatabaseHandler(getActivity())), new EventsLocationsRepository(new DatabaseHandler(getActivity()))),
                new GetFilteredEventsUseCase(new EventRepository(getActivity(), new DatabaseHandler(getActivity()))),
                new GetFutureEventsUseCase(new EventRepository(getActivity(), new DatabaseHandler(getActivity())), new DateService()),
                new AddOrModifyLocationUseCase(new LocationsRepository(new DatabaseHandler(getActivity()))),
                new LocationsRepository(new DatabaseHandler(getActivity())),
                new GetEventForLocationUseCase(new EventsLocationsRepository(new DatabaseHandler(getActivity())),
                        new EventRepository(getActivity(), new DatabaseHandler(getActivity())), null));
        locationsPresenter.attachView(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        locationsPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationsPresenter.onConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        locationsPresenter.onLocationChanged();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        locationsPresenter.onMapReady();
    }



    public void toastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void deleteMarkers() {
        mMap.clear();
        updateCurrentLocation();
    }

    @Override
    public void displayEventListFragment(List<Event> events) {
        EventListFragment eventListFragment = EventListFragment.newInstance(events);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content_map,eventListFragment).addToBackStack(EventListFragment.TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void initializeMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google play services
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED){
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    @Override
    public void updateCurrentLocation() {
        if(mCurrLocationMarker != null){
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(yourLocation);

        //stop location updates
        if(mGoogleApiClient != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    //GoogleMap
    public void initializeMapClickListener(){
//        final RouteHelper routeHelper = new RouteHelper(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //compute distance
//                try {
//                    int distance = (int) SphericalUtil.computeDistanceBetween(mCurrLocationMarker.getPosition(), latLng);
//                    if (distance > 10000)
//                        toastMessage("Distance to your current position is " + String.valueOf(distance / 1000) + " km.");
//                    else
//                        toastMessage("Distance to your current position is " + String.valueOf(distance) + " m.");
//                }catch (Exception e){
//                    Log.d("INFO", e.getMessage());
//                }
                //new location
                //locationsPresenter.pickPlace();
//                routeHelper.TimeFromPointToPoint(mCurrLocationMarker.getPosition(), latLng);
            }
        });
    }

    @Override
    public void getLocations(List<CustomerLocation> locations) {
        if(locations != null)
            for(int i = 0; i < locations.size(); i++){
                LatLng latLng = new LatLng(locations.get(i).getLat(), locations.get(i).getLng());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(locations.get(i).getName());
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                mMap.addMarker(markerOptions).setTag(locations.get(i).getId());

            }
    }

    @Override
    public void initializeLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void locationPermissionEnable(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_LOCATION:{
                //If request is cancelled, the result arrays are empty.
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Permission was granted.
                    if(ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                        if(mGoogleApiClient == null){
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }else {
                    //Pemission denied, Disable the functionality that depends on this permission.
                    toastMessage("permission denied");
                }
                return;
            }
            //other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void startPlacePickerDialog() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(getActivity());
            startActivityForResult(intent, MapsActivity.RequestCode.PICK_LOCATION);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MapsActivity.RequestCode.PICK_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                locationsPresenter.insert(place.getAddress().toString(), place.getLatLng().latitude, place.getLatLng().longitude);
            }
        }

    }

    public void initializeMarkerClickListener(){
        final Dialog dialog =  new Dialog(getActivity());

        dialog.setContentView(R.layout.dialog_delete_location);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                if(marker.getTag() != null) {
//                    dialog.setTitle(marker.getTitle());
//                    dialog.show();
//
//                    Button deleteButton = (Button) dialog.findViewById(R.id.deleteButton);
//                    deleteButton.setText(R.string.delete_dialog_button);
//                    deleteButton.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                           // repository.delete((Long)marker.getTag());
//                            locationsPresenter.delete((long)marker.getTag());
//                            marker.remove();
//                            dialog.dismiss();
//                        }
//                    });
//
//                    Button exitButton = (Button) dialog.findViewById(R.id.exitButton);
//                    exitButton.setText(R.string.exit_dialog_button);
//                    exitButton.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });

                locationsPresenter.onLocationClick((Long)marker.getTag());
                }
                return true;
            }
        });
    }

    public void addMarker(long id, String name, double lat, double lng) {

        final MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lng));
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        marker.title(name);
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        mMap.addMarker(marker).setTag(id);

    }

    @Override
    public void displayEventDetailsFragment(Event event) {
        EventDetailsFragment frag = EventDetailsFragment.newInstance(event);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.content_map, frag, EventDetailsFragment.TAG).addToBackStack(EventDetailsFragment.TAG);
        transaction.commit();
    }

    @Override
    public void displayTimeFilterFragment() {
        TimeFilterFragment frag = TimeFilterFragment.newInstance();
        frag.attachListener(this);
        frag.show(getActivity().getSupportFragmentManager(), "datePicker");
    }


    public boolean checkLocationPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            //Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Show an explanation to the user *asynchronously* -- don't block
                //this thread waiting for the user's response! After the user
                //the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                //No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        }else{
            return true;
        }
    }


}