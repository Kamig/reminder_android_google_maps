package pl.edu.pw.mini.grastudio.reminder.model;

/**
 * Created by Kamil on 16.05.2017.
 */

public class EventLocationContener extends Entity {
    Event event;
    CustomerLocation location;

    public EventLocationContener(Event event, CustomerLocation location) {
        super(-1);
        this.event = event;
        this.location = location;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public CustomerLocation getLocation() {
        return location;
    }

    public void setLocation(CustomerLocation location) {
        this.location = location;
    }
}
