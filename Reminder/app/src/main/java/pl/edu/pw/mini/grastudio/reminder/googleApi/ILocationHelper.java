package pl.edu.pw.mini.grastudio.reminder.googleApi;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by PC DELL on 23.05.2017.
 */

public interface ILocationHelper extends GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    void initializeLocationRequest();

    Location getCurrentLocation();
    LatLng getCurrentLatLng();
}
