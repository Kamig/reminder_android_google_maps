package pl.edu.pw.mini.grastudio.reminder.presenter;


import android.util.Log;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyEventLocationLinkUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetLinkForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.utils.DateUtils;
import pl.edu.pw.mini.grastudio.reminder.view.IAddEventView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class AddEventPresenter implements IAddEventPresenter {
    private IAddEventView view;
    private Subscription addOrUpdateEventSubscription;
    private Subscription addOrUpdateLocationSubscription;
    private Subscription getAddOrUpdateEventLocationSubscription;
    private Subscription getLocationForEventSubscription;
    private Subscription getLocationEventLinkSubscription;

    private IAddOrModifyEventUseCase addOrModifyEventUseCase;
    private IAddOrModifyEventLocationLinkUseCase addOrModifyEventLocationLinkUseCase;
    private IAddOrModifyLocationUseCase addOrModifyLocationUseCase;
    private IGetLocationForEventUseCase getLocationForEventUseCase;
    private IGetLinkForEventUseCase getLinkForEventUseCase;
    private CustomerLocation location;
    private EventLocationLink eventLocationLink;
    private Date startDate;
    private Date endDate;
    private boolean isAllDay;
    private int durationMinutes;
    private int durationHours;
    private long id;
    private String title;
    private Event.Priority priority;
    private Map<String, Event.Priority> priorityMap;

    private final int DATE_FIRST_YEAR = 1900;
    private final int MINUTES_PER_HOUR = 60;
    private final int MILLISECONDS_PER_MINUTE = 60 * 1000;
    private final double DEFAULT_DISTANCE = 30;

    public AddEventPresenter(IAddOrModifyEventUseCase addOrModifyEventUseCase,
                             IAddOrModifyEventLocationLinkUseCase addOrModifyEventLocationLinkUseCase,
                             IAddOrModifyLocationUseCase addOrModifyLocationUseCase,
                             IGetLocationForEventUseCase getLocationForEventUseCase,
                             IGetLinkForEventUseCase getLinkForEventUseCase) {

        this.addOrModifyEventUseCase = addOrModifyEventUseCase;
        this.addOrModifyEventLocationLinkUseCase = addOrModifyEventLocationLinkUseCase;
        this.addOrModifyLocationUseCase = addOrModifyLocationUseCase;
        this.getLocationForEventUseCase = getLocationForEventUseCase;
        this.getLinkForEventUseCase = getLinkForEventUseCase;
        id = -1;
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        //stop getting events
        if (addOrUpdateEventSubscription != null && !addOrUpdateEventSubscription.isUnsubscribed()) {
            addOrUpdateEventSubscription.unsubscribe();
        }

        if (addOrUpdateLocationSubscription != null && !addOrUpdateLocationSubscription.isUnsubscribed()) {
            addOrUpdateLocationSubscription.unsubscribe();
        }

        if (getAddOrUpdateEventLocationSubscription != null && !getAddOrUpdateEventLocationSubscription.isUnsubscribed()) {
            getAddOrUpdateEventLocationSubscription.unsubscribe();
        }

        if (getLocationForEventSubscription != null && !getLocationForEventSubscription.isUnsubscribed()) {
            getLocationForEventSubscription.unsubscribe();
        }

        if (getLocationEventLinkSubscription != null && !getLocationEventLinkSubscription.isUnsubscribed()) {
            getLocationEventLinkSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IAddEventView view) {
        this.view = view;

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        Date currentDate = new Date();
        startDate = new Date();
        endDate = new Date();
        eventLocationLink = new EventLocationLink(-1, -1, -1);

        view.loadDateAndTime(dateFormat.format(currentDate), timeFormat.format(currentDate));
        setDistance(DEFAULT_DISTANCE);
    }

    @Override
    public void pickNewStartDate(String currentDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
        try {
            Date parsedDate = dateFormat.parse(currentDate);
            view.showStartDateDialog(parsedDate.getYear() + DATE_FIRST_YEAR,
                    parsedDate.getMonth(), DateUtils.getDayOfTheMonth(parsedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNewStartDate(int year, int month, int dayOfMonth) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
        startDate = new Date(year - DATE_FIRST_YEAR, month, dayOfMonth, startDate.getHours(), startDate.getMinutes());
        view.setStartDate(dateFormat.format(startDate));
        if (startDate.after(endDate)) {
            endDate = new Date(startDate.getYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes());
            setNewEndDate(endDate.getYear() + DATE_FIRST_YEAR, endDate.getMonth(), endDate.getDate());
        }
    }

    @Override
    public void pickNewEndDate(String currentDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
        try {
            Date parsedDate = dateFormat.parse(currentDate);
            view.showEndDateDialog(parsedDate.getYear() + DATE_FIRST_YEAR,
                    parsedDate.getMonth(), DateUtils.getDayOfTheMonth(parsedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNewEndDate(int year, int month, int dayOfMonth) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy");
        endDate = new Date(year - DATE_FIRST_YEAR, month, dayOfMonth, endDate.getHours(), endDate.getMinutes());
        view.setEndDate(dateFormat.format(endDate));
        if (endDate.before(startDate)) {
            startDate = new Date(endDate.getYear(), endDate.getMonth(), endDate.getDate(), endDate.getHours(), endDate.getMinutes());
            setNewStartDate(startDate.getYear() + DATE_FIRST_YEAR, startDate.getMonth(), startDate.getDate());
        }
    }

    @Override
    public void pickNewStartTime(String currentTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        try {
            Date parsedTime = dateFormat.parse(currentTime);
            view.showStartTimeDialog(parsedTime.getHours(), parsedTime.getMinutes());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNewStartTime(int hour, int minute) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        startDate = new Date(startDate.getYear(), startDate.getMonth(), startDate.getDate(), hour, minute);
        view.setStartTime(dateFormat.format(startDate));
        if (startDate.after(endDate)) {
            endDate = new Date(startDate.getYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes());
            setNewEndTime(endDate.getHours(), endDate.getMinutes());
        }
    }

    @Override
    public void pickNewEndTime(String currentTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        try {
            Date parsedTime = dateFormat.parse(currentTime);
            view.showEndTimeDialog(parsedTime.getHours(), parsedTime.getMinutes());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNewEndTime(int hour, int minute) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        endDate = new Date(endDate.getYear(), endDate.getMonth(), endDate.getDate(), hour, minute);
        view.setEndTime(dateFormat.format(endDate));
        if (endDate.before(startDate)) {
            startDate = new Date(endDate.getYear(), endDate.getMonth(), endDate.getDate(), endDate.getHours(), endDate.getMinutes());
            setNewStartTime(startDate.getHours(), startDate.getMinutes());
        }
    }

    @Override
    public void onAllDayCheckChanged(boolean isAllDay) {
        this.isAllDay = isAllDay;
        if (isAllDay) {
            int minutes = (int) Math.ceil((double) (endDate.getTime() - startDate.getTime()) / (double) MILLISECONDS_PER_MINUTE);
            int hours = minutes / MINUTES_PER_HOUR;
            minutes = minutes % MINUTES_PER_HOUR;
            view.showDurationFragment(hours, minutes);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            view.showTimeFragment(dateFormat.format(startDate), dateFormat.format(endDate));
        }
    }

    @Override
    public void addOrUpdateEvent(String name, String startDate, String endDate, String startTime, String endTime,
                                 boolean isAllDay, int durationHours, int durationMinutes, int selectedPriorityIndex,
                                 String distance, int selectedTransportIndex) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("EEE, dd.MM.yyyy HH:mm");
        Date parsedStartDateTime = this.startDate;
        Date parsedEndDateTime = this.endDate;

        Event.Priority priority = Event.Priority.values()[selectedPriorityIndex];

        final Event event = new Event(name, parsedStartDateTime, parsedEndDateTime, isAllDay, priority);
        event.setId(id);
        event.setDurationHours(durationHours);
        event.setDurationMinutes(durationMinutes);
        event.setMeanOfTransport(selectedTransportIndex);
        if(distance == ""){
            event.setReminderDistanceMeters(DEFAULT_DISTANCE);
        }
        else {
            event.setReminderDistanceMeters(Double.parseDouble(distance));
        }
        addOrModifyEventUseCase.setEventToAdd(event);
        addOrModifyLocationUseCase.setLocationToAdd(location);
        addOrUpdateEventSubscription = addOrModifyEventUseCase.execute().zipWith(addOrModifyLocationUseCase.execute(),
                new Func2<Long, Long, Boolean>() {

                    @Override
                    public Boolean call(Long eventId, Long locationId) {
                        Log.i("ADD", "Id is: " + eventId.toString());
                        event.setId(eventId);
                        if(locationId == -1)
                            return false;
                        location.setId(locationId);
                        if(eventLocationLink == null)
                            eventLocationLink = new EventLocationLink(-1, -1, -1);
                        eventLocationLink.setEventId(eventId);
                        eventLocationLink.setLocationId(locationId);
                        addOrModifyEventLocationLinkUseCase.setLinkToAdd(eventLocationLink);
                        return true;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean shouldAddLink) {
                        if(!shouldAddLink) {
                            view.returnToLastScreenWithResult(new Gson().toJson(event));
                        }
                        else {
                            getAddOrUpdateEventLocationSubscription = addOrModifyEventLocationLinkUseCase.execute()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturn(new Func1<Throwable, Long>() {
                                        @Override
                                        public Long call(Throwable throwable) {
                                            //there was an error while getting events
                                            throwable.printStackTrace();
                                            return null;
                                        }
                                    })
                                    .subscribe(new Action1<Long>() {
                                        @Override
                                        public void call(Long id) {
                                            //not necessary
                                            eventLocationLink.setId(id);
                                            Log.i("ADD", "Added link");
                                            view.returnToLastScreenWithResult(new Gson().toJson(event));
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void cancelEditing() {
        view.returnToLastScreen();
    }

    @Override
    public void pickPlace() {
        view.startPlacePickerDialog();
    }

    @Override
    public void onPlaceSelected(long id, String name, String address, String latLng, double lat, double lng, boolean isFav) {
        //this.place = place;
        String text = name;
        if (address != "")
            text = text + ", " + address;
        if (text.length() == 0)
            text = latLng;
        view.setLocationText(text);

        location = new CustomerLocation(id, text, lat, lng, isFav);
    }


    @Override
    public void setNewDuration(int durationHours, int durationMinutes) {
        this.durationMinutes = durationMinutes;
        this.durationHours = durationHours;
        view.setDuration(durationHours, durationMinutes);
    }

    @Override
    public void newTitle(String title) {
        this.title = title;
        view.setTitle(title);
    }

    @Override
    public void setEvent(final Event event) {
        if (event != null) {
            id = event.getId();
            view.setAllDay(event.isAllDay());
            newTitle(event.getName());
            setNewStartDate(event.getStartDate().getYear() + DATE_FIRST_YEAR,
                    event.getStartDate().getMonth(),
                    event.getStartDate().getDate());
            setNewEndDate(event.getEndDate().getYear() + DATE_FIRST_YEAR,
                    event.getEndDate().getMonth(),
                    event.getEndDate().getDate());
            if (event.isAllDay()) {
                setNewDuration(event.getDurationHours() , event.getDurationMinutes());
            } else {
                setNewStartTime(event.getStartDate().getHours(), event.getStartDate().getMinutes());
                setNewEndTime(event.getEndDate().getHours(), event.getEndDate().getMinutes());
            }
            onAllDayCheckChanged(event.isAllDay());
            setPriority(event.getPriority());
            setTransportIndex(event.getTransportInt());
            setDistance(event.getReminderDistanceMeters());

            getLocationForEventUseCase.setEvent(event);
            getLocationForEventSubscription = getLocationForEventUseCase.execute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                        @Override
                        public CustomerLocation call(Throwable throwable) {
                            throwable.printStackTrace();
                            return null;
                        }
                    })
                    .subscribe(new Action1<CustomerLocation>() {
                        @Override
                        public void call(CustomerLocation customerLocation) {
                            if(customerLocation == null)
                                return;
                            onPlaceSelected(customerLocation.getId(), customerLocation.getName(), "",
                                    Double.toString(customerLocation.getLat()) + ", " + Double.toString(customerLocation.getLng()),
                                    customerLocation.getLat(),
                                    customerLocation.getLng(),
                                    customerLocation.isFavorite());
                        }
                    });
            getLinkForEventUseCase.setEvent(event);
            getLocationEventLinkSubscription = getLinkForEventUseCase.execute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorReturn(new Func1<Throwable, EventLocationLink>() {
                        @Override
                        public EventLocationLink call(Throwable throwable) {
                            throwable.printStackTrace();
                            return null;
                        }
                    })
                    .subscribe(new Action1<EventLocationLink>() {
                        @Override
                        public void call(EventLocationLink link) {
                            eventLocationLink = link;
                        }
                    });

        }
    }

    private void setDistance(double reminderDistanceMeters) {
        view.setDistance(reminderDistanceMeters);
    }

    private void setTransportIndex(int transportInt) {
        view.setTransport(transportInt);
    }

    @Override
    public void initPriority(String[] priorityNames) {
        priorityMap = new HashMap<>();
        for(int i = 0; i < priorityNames.length; i++) {
            priorityMap.put(priorityNames[i], Event.Priority.values()[i]);
        }
    }

    private void setPriority(Event.Priority priority) {
        this.priority = priority;
        view.setSelectedPriority(Arrays.asList(Event.Priority.values()).indexOf(priority));
    }
}
