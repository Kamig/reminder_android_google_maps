package pl.edu.pw.mini.grastudio.reminder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import pl.edu.pw.mini.grastudio.reminder.adapter.DateAdapter;
import pl.edu.pw.mini.grastudio.reminder.adapter.EventsAdapter;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.DeleteEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetFutureEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.presenter.EventsPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IEventsPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.DateService;
import pl.edu.pw.mini.grastudio.reminder.view.IEventsView;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.R.id.message;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends Fragment
        implements IEventsView, EventsAdapter.OnEventDeletedListener, EventsAdapter.OnClickedListener,
                    DateAdapter.OnClickedListener{

    private DateAdapter dateAdapter;
    private IEventsPresenter eventsPresenter;
    private RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.datesRecycleView)
    public RecyclerView datesRecycleView;
    public FloatingActionButton floatingActionButton;

//    @BindView(R.id.eventListProgress)
//    public ProgressBar eventProgress;



    class RequestCode {
        static final int ADD_ACTIVITY = 1;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());
        initializePresenter();
        initializeRecycleView();

        floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventsPresenter.floatingActionButtonOnClick();
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.content_main, container, false);
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();

        eventsPresenter.onStart();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && getActivity() != null) {
            if(floatingActionButton == null)
                floatingActionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
            floatingActionButton.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_add_white_24dp));
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventsPresenter.floatingActionButtonOnClick();
                }
            });
        }
    }

    @Override
    public void onStop() {
        eventsPresenter.onStop();
        super.onStop();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.ADD_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                String serializedData = data.getStringExtra(AddEventActivity.ARG_EVENT);
                eventsPresenter.onEventAdded(serializedData);

            }
        }
    }

    @Override
    public void onDeleted(Event event) {
        eventsPresenter.onEventDeleted(event);
    }

    @Override
    public void onClicked(Event event) {
            String oldTitle = getActivity().getTitle().toString();
            EventDetailsFragment frag = EventDetailsFragment.newInstance(event, oldTitle, true);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.setCustomAnimations(R.anim.anim_in, R.anim.anim_out);
            transaction.replace(R.id.content, frag, EventDetailsFragment.TAG).addToBackStack(EventDetailsFragment.TAG);
            transaction.commit();
    }

    @Override
    public void onClicked(Date date) {
        DayScheduleFragment frag = DayScheduleFragment.newInstance(date);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.setCustomAnimations(R.anim.anim_in, R.anim.anim_out);
        transaction.replace(R.id.content, frag, DayScheduleFragment.TAG).addToBackStack(DayScheduleFragment.TAG);
        transaction.commit();
    }

    @Override
    public void showEmpty() {
        //TODO: show message, that there are no events
    }

    @Override
    public void showEvents(HashMap<String, List<Event>> events) {
        dateAdapter.replaceData(events);
//        datesRecycleView.swapAdapter(dateAdapter, true);
//        datesRecycleView.setLayoutManager(layoutManager);
//        dateAdapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToAddEventView() {
        Intent intent = new Intent(getActivity(), AddEventActivity.class);
        startActivityForResult(intent, RequestCode.ADD_ACTIVITY);
    }

    @Override
    public void setLoading(boolean isLoading) {
//        eventProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void initializePresenter() {
        //TODO: maybe use injection when I finally understand it
        eventsPresenter = new EventsPresenter(new GetFutureEventsUseCase(new EventRepository(getActivity(), new DatabaseHandler(getActivity())), new DateService()),
                new DeleteEventUseCase(new EventRepository(getActivity(), new DatabaseHandler(getActivity())), new EventsLocationsRepository(new DatabaseHandler(getActivity()))));
        eventsPresenter.attachView(this);
    }

    private void initializeRecycleView() {
        //this improves performance
        datesRecycleView.setHasFixedSize(true);
        datesRecycleView.setNestedScrollingEnabled(false);
        //linear manager -> a list!
        layoutManager = new LinearLayoutManager(getActivity());
        datesRecycleView.setLayoutManager(layoutManager);
        dateAdapter = new DateAdapter(getActivity(), this);
        dateAdapter.attachListener(this);
        dateAdapter.attachDeleteListener(this);
        datesRecycleView.setAdapter(dateAdapter);
    }



}
