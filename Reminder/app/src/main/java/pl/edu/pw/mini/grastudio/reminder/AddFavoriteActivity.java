package pl.edu.pw.mini.grastudio.reminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.AddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.presenter.AddFavoritePresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IAddFavoritePresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.IAddFavouriteView;

public class AddFavoriteActivity extends AppCompatActivity implements IAddFavouriteView {

    IAddFavoritePresenter presenter;

    class RequestCode {
        static final int PICK_LOCATION = 1;
    }

    public static final String ARG_LOCATION = "location";

    @BindView(R.id.locationName)
    EditText locationNameEditText;
    @BindView(R.id.locationTextView)
    Button selectedLocationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_favorite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        initializePresenter();
    }

    private void initializePresenter() {
        presenter = new AddFavoritePresenter(new AddOrModifyLocationUseCase(new LocationsRepository(new DatabaseHandler(this))));
        presenter.attachView(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_add) {

            boolean nameError = false;
            boolean locationError = false;
            if(locationNameEditText.getText().toString().length() == 0) {
                locationNameEditText.setError(getResources().getString(R.string.name_empty));
                nameError = true;
            }
            else {
                locationNameEditText.setError(null);
            }
            if(!presenter.hasLocation()) {
                selectedLocationTextView.setError("");
                locationError = true;
            }
            else {
                selectedLocationTextView.setError(null);
            }
            if(!locationError && !nameError) {
                presenter.addOrUpdateFavorite(locationNameEditText.getText().toString());
            }
        }
        if(id == R.id.action_back) {
            presenter.cancelEditing();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onLocationClick(View v) {
        presenter.pickPlace();
    }

    @Override
    public void startPlacePickerDialog() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(this);
            startActivityForResult(intent, RequestCode.PICK_LOCATION);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setLocationText(String text) {
        selectedLocationTextView.setText(text);
        selectedLocationTextView.setError(null);
    }

    @Override
    public void returnToLastScreen() {
        finish();
    }

    @Override
    public void returnToLastScreenWithResult(String serializedResult) {
        Intent data = new Intent();
        data.putExtra(ARG_LOCATION, serializedResult);
        setResult(RESULT_OK, data);
        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.PICK_LOCATION) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                presenter.onPlaceSelected(place.getName().toString(), place.getAddress().toString(),
                        place.getLatLng().toString(), place.getLatLng().latitude, place.getLatLng().longitude );
            }
        }
    }
}
