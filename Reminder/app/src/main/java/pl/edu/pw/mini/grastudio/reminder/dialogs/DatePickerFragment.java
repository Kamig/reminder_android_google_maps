package pl.edu.pw.mini.grastudio.reminder.dialogs;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {

    private static final String INITIAL_YEAR = "INITIAL_YEAR";
    private static final String INITIAL_MONTH = "INITIAL_MONTH";
    private static final String INITIAL_DAY = "INITIAL_DAY";

    OnDatePickedListener listener;

    public static DatePickerFragment newInstance(int year, int month, int day, OnDatePickedListener listener) {

        Bundle args = new Bundle();
        args.putInt(INITIAL_YEAR, year);
        args.putInt(INITIAL_MONTH, month);
        args.putInt(INITIAL_DAY, day);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.listener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year = getArguments().getInt(INITIAL_YEAR);
        int month = getArguments().getInt(INITIAL_MONTH);
        int day = getArguments().getInt(INITIAL_DAY);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if(listener != null) {
            listener.onDatePicked(year, month, dayOfMonth);
        }
    }

    public interface OnDatePickedListener {
        void onDatePicked(int year, int month, int dayOfMonth);
    }
}
