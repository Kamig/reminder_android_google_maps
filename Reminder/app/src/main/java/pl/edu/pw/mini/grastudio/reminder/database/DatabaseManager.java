package pl.edu.pw.mini.grastudio.reminder.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager {

    private static DatabaseManager instance;
    private static SQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    public static synchronized DatabaseManager getInstance(SQLiteOpenHelper helper) {
        if (instance == null) {
            instance = new DatabaseManager();
            mDatabaseHelper = helper;
        }

        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        if(mDatabase == null)
            mDatabase = mDatabaseHelper.getWritableDatabase();
        return mDatabase;
    }
}
