package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

public interface IGetLocationForEventUseCase extends IUseCase<CustomerLocation> {
    void setEvent(Event event);
}
