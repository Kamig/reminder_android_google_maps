package pl.edu.pw.mini.grastudio.reminder.presenter;



import android.util.Log;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.GetFilteredEventsUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IAddOrModifyLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetEventForLocationUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IGetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.domain.IUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.view.ILocationsView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * Created by Kamil on 14.04.2017.
 */

public class LocationsPresenter implements ILocationsPresenter  {


    //IPresenter
    private ILocationsView locationsView;
    private IRepository<CustomerLocation> locationsRepository;

    private IGetEventForLocationUseCase getEventForLocationUseCase;
    private Subscription getEventLocationsSubscription;
    private Subscription getEventFromLocationSubscription;
    private IUseCase<CustomerLocation> getLocationFromEvent;
    private IUseCase<List<Event>> getFilteredEventsUseCase;
    private IUseCase<List<Event>> getFutureEventsUseCase;
    private IUseCase<Long> addOrModifyLocationUseCase;

    public LocationsPresenter(IUseCase<CustomerLocation> getLocationForEvent, IUseCase<List<Event>> getFilteredEventsUseCase, IUseCase<List<Event>> getFutureEventsUseCase,
                              IUseCase<Long> addOrModifyLocationUseCase, IRepository<CustomerLocation> locationsRepository, IGetEventForLocationUseCase getEventForLocationUseCase) {
        this.locationsRepository = locationsRepository;
        this.getEventForLocationUseCase = getEventForLocationUseCase;
        this.getLocationFromEvent = getLocationForEvent;
        this.getFilteredEventsUseCase = getFilteredEventsUseCase;
        this.getFutureEventsUseCase = getFutureEventsUseCase;
        this.addOrModifyLocationUseCase = addOrModifyLocationUseCase;
    }

    //IPresenter
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        //stop getting events
        if(getEventLocationsSubscription != null && !getEventLocationsSubscription.isUnsubscribed()) {
            getEventLocationsSubscription.unsubscribe();
        }
        if(getEventFromLocationSubscription != null && !getEventFromLocationSubscription.isUnsubscribed()) {
            getEventFromLocationSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    public void pickPlace(){
        locationsView.startPlacePickerDialog();
    }

    @Override
    public void attachView(ILocationsView view) {
        this.locationsView = view;
    }


    public void onMapReady() {
        locationsView.initializeMap();
        locationsView.initializeMapClickListener();
        locationsView.initializeMarkerClickListener();

        Log.d("INFO", "OnMapReady");

        getFutureEventsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(List<Event> events) {
                        for(int i = 0; i < events.size(); i++){

                            ((IGetLocationForEventUseCase)getLocationFromEvent).setEvent(events.get(i));
                            getLocationFromEvent.execute()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                                        @Override
                                        public CustomerLocation call(Throwable throwable) {
                                            //there was an error while getting events
                                            throwable.printStackTrace();
                                            return null;
                                        }
                                    })
                                    .subscribe(new Action1<CustomerLocation>() {
                                        @Override
                                        public void call(CustomerLocation location) {
                                            // locations.add(location);
                                            if(location != null)
                                            locationsView.addMarker(location.getId(), location.getName(), location.getLat(), location.getLng());
                                        }
                                    });
                        }
                    }
                });

    }

    public void OnlyTodayEvents() {
        locationsView.displayTimeFilterFragment();
//        locationsView.deleteMarkers();
//        getTodayEventsUseCase.execute()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorReturn(new Func1<Throwable, List<Event>>() {
//                    @Override
//                    public List<Event> call(Throwable throwable) {
//                        throwable.printStackTrace();
//                        return null;
//                    }
//                })
//                .subscribe(new Action1<List<Event>>() {
//                    @Override
//                    public void call(List<Event> events) {
//                        for(int i = 0; i < events.size(); i++){
//
//
//                            ((IGetLocationForEventUseCase)getLocationFromEvent).setEvent(events.get(i));
//                            getLocationFromEvent.execute()
//                                    .subscribeOn(Schedulers.io())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
//                                        @Override
//                                        public CustomerLocation call(Throwable throwable) {
//                                            //there was an error while getting events
//                                            throwable.printStackTrace();
//                                            return null;
//                                        }
//                                    })
//                                    .subscribe(new Action1<CustomerLocation>() {
//                                        @Override
//                                        public void call(CustomerLocation location) {
//                                            // locations.add(location);
//                                            if(location != null)
//                                            locationsView.addMarker(location.getId(), location.getName(), location.getLat(), location.getLng());
//                                        }
//                                    });
//                        }
//                    }
//                });
    }

    @Override
    public void onLocationClick(long locationId) {
        getEventForLocationUseCase.setId(locationId);
        getEventFromLocationSubscription = getEventForLocationUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(List<Event> events) {
                        locationsView.displayEventListFragment(events);
                    }
                });
    }

    @Override
    public void updateMarkers() {
        getFutureEventsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(List<Event> events) {
                        locationsView.deleteMarkers();
                        for(int i = 0; i < events.size(); i++){

                            ((IGetLocationForEventUseCase)getLocationFromEvent).setEvent(events.get(i));
                            getLocationFromEvent.execute()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                                        @Override
                                        public CustomerLocation call(Throwable throwable) {
                                            //there was an error while getting events
                                            throwable.printStackTrace();
                                            return null;
                                        }
                                    })
                                    .subscribe(new Action1<CustomerLocation>() {
                                        @Override
                                        public void call(CustomerLocation location) {
                                            // locations.add(location);
                                            if(location != null)
                                                locationsView.addMarker(location.getId(), location.getName(), location.getLat(), location.getLng());
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void filterEvents(Date startDate, Date endDate) {
        locationsView.deleteMarkers();
        ((GetFilteredEventsUseCase)getFilteredEventsUseCase).setStartDate(startDate);
        ((GetFilteredEventsUseCase)getFilteredEventsUseCase).setEndDate(endDate);
        getFilteredEventsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, List<Event>>() {
                    @Override
                    public List<Event> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<Event>>() {
                    @Override
                    public void call(List<Event> events) {
                        for(int i = 0; i < events.size(); i++){


                            ((GetLocationForEventUseCase)getLocationFromEvent).setEvent(events.get(i));
                            getLocationFromEvent.execute()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturn(new Func1<Throwable, CustomerLocation>() {
                                        @Override
                                        public CustomerLocation call(Throwable throwable) {
                                            //there was an error while getting events
                                            throwable.printStackTrace();
                                            return null;
                                        }
                                    })
                                    .subscribe(new Action1<CustomerLocation>() {
                                        @Override
                                        public void call(CustomerLocation location) {
                                            // locations.add(location);
                                            if(location != null)
                                                locationsView.addMarker(location.getId(), location.getName(), location.getLat(), location.getLng());
                                        }
                                    });
                        }
                    }
                });
    }


    @Override
    public void onLocationChanged() {
        locationsView.updateCurrentLocation();
    }

    @Override
    public void onConnected() {
        locationsView.initializeLocationRequest();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        locationsView.locationPermissionEnable(requestCode, permissions, grantResults);
    }

    public void delete(long id){
        locationsRepository.delete(id);
    }

    @Override
    public void insert(final String name, final double lat, final double lng) {

        ((IAddOrModifyLocationUseCase)addOrModifyLocationUseCase).setLocationToAdd(new CustomerLocation(-1, name, lat, lng));
                        addOrModifyLocationUseCase.execute()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .onErrorReturn(new Func1<Throwable, Long>() {
                            @Override
                            public Long call(Throwable throwable) {
                                //there was an error while getting events
                                throwable.printStackTrace();
                                return null;
                            }
                        })
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long id) {
                                locationsView.addMarker(id, name, lat, lng);
                            }
                        });
    }
}
