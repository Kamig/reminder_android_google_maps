package pl.edu.pw.mini.grastudio.reminder.repository;

import java.util.List;
import java.util.concurrent.Callable;

import pl.edu.pw.mini.grastudio.reminder.database.IDatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import rx.Observable;

/**
 * Created by Kamil on 15.04.2017.
 */

public class LocationsRepository implements IRepository<CustomerLocation> {

    private IDatabaseHandler db;

    public LocationsRepository(IDatabaseHandler db) {
        this.db = db;
    }

    @Override
    public Observable<CustomerLocation> getById(final long id) {
        return Observable.fromCallable(new Callable<CustomerLocation>() {
            @Override
            public CustomerLocation call() throws Exception {
                return db.getLocation(id);
            }
        });
    }

    @Override
    public Observable<List<CustomerLocation>> getAll() {
        return Observable.fromCallable(new Callable<List<CustomerLocation>>() {
            @Override
            public List<CustomerLocation> call() throws Exception {
                return db.getAllLocations();
            }
        });
    }

    @Override
    public Observable<Long> addOrUpdate(final CustomerLocation item) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                if(item.getId() == -1)
                    return db.addLocation(item);
                else
                    return db.updateLocation(item);
            }
        });
    }

    @Override
    public Observable<Integer> delete(final long id) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return db.deleteLocation(id);
            }
        });
    }
}
