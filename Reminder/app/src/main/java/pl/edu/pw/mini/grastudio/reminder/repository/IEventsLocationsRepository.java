package pl.edu.pw.mini.grastudio.reminder.repository;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import rx.Observable;

public interface IEventsLocationsRepository extends IRepository<EventLocationLink> {
    Observable<EventLocationLink> getByEventId(long id);

    Observable<List<EventLocationLink>> getByLocationId(long id);
}
