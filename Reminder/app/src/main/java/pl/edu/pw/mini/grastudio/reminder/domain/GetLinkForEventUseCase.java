package pl.edu.pw.mini.grastudio.reminder.domain;


import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import rx.Observable;

public class GetLinkForEventUseCase implements IGetLinkForEventUseCase {
    private Event event;
    private IEventsLocationsRepository eventsLocationsRepository;

    public GetLinkForEventUseCase(IEventsLocationsRepository eventsLocationsRepository) {
        this.eventsLocationsRepository = eventsLocationsRepository;
    }

    @Override
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public Observable<EventLocationLink> execute() {
        return eventsLocationsRepository.getByEventId(event.getId());
    }
}
