package pl.edu.pw.mini.grastudio.reminder.domain;

public interface IDeleteEventUseCase extends IUseCase<Integer> {
    void setId(long id);
}
