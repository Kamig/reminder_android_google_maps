package pl.edu.pw.mini.grastudio.reminder.domain;

import rx.Observable;

public interface IUseCase<T> {
    Observable<T> execute();
}
