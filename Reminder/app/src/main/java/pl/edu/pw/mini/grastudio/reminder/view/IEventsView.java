package pl.edu.pw.mini.grastudio.reminder.view;

import java.util.HashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;

public interface IEventsView extends View {
    void showEmpty();
    void showEvents(HashMap<String, List<Event>> events);
    void navigateToAddEventView();
    void setLoading(boolean isLoading);
}
