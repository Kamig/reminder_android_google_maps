package pl.edu.pw.mini.grastudio.reminder;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.database.DatabaseHandler;
import pl.edu.pw.mini.grastudio.reminder.domain.GetLocationForEventUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.presenter.EventDetailsPresenter;
import pl.edu.pw.mini.grastudio.reminder.presenter.IEventDetailsPresenter;
import pl.edu.pw.mini.grastudio.reminder.repository.EventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.LocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.view.IEventDetailsView;


public class EventDetailsFragment extends Fragment implements IEventDetailsView, OnMapReadyCallback {

    //Used to pass arguments to the fragment
    private static final String EVENT_PARAM = "EVENT";
    private static final String TITLE_PARAM = "TITLE";
    private static final String TABS_PARAM = "TABS";

    private IEventDetailsPresenter presenter;
    //Tag for fragment
    public static final String TAG = "EVENT_FRAG";
    @BindView(R.id.dateTextView)
    public TextView dateTextView;
    @BindView(R.id.timeTextView)
    public TextView timeTextView;
    @BindView(R.id.locationTextView)
    public  TextView locationTextView;
    private GoogleMap mMap;

    Event event;
    //the old title that we need to set again when we close the fragment
    private String oldTitle;
    private boolean showTabs;

    private Menu menu;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng latLng = presenter.getLatLngLocation();
        if(latLng == null)
            return;
        mMap.addMarker(new MarkerOptions().position(latLng).title(getResources().getString(R.string.marker_name)));
        //move map camera
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(yourLocation);
    }

    class RequestCode {
        static final int EDIT_ACTIVITY = 1;
    }


    public EventDetailsFragment() {
        // Required empty public constructor
    }

    public static EventDetailsFragment newInstance(Event event) {
        EventDetailsFragment fragment = new EventDetailsFragment();
        Bundle args = new Bundle();
        String serializedEvent = (new Gson().toJson(event));
        args.putString(EventDetailsFragment.EVENT_PARAM, serializedEvent);
        fragment.setArguments(args);
        return fragment;
    }

    public static EventDetailsFragment newInstance(Event event, String oldTitle, boolean showTabs) {
        EventDetailsFragment fragment = newInstance(event);
        fragment.getArguments().putString(TITLE_PARAM, oldTitle);
        fragment.getArguments().putBoolean(TABS_PARAM, showTabs);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String serializedEvent = getArguments().getString(EVENT_PARAM);
            event = new Gson().fromJson(serializedEvent, Event.class);
            oldTitle = getArguments().getString(TITLE_PARAM, null);
            showTabs = getArguments().getBoolean(TABS_PARAM);
            presenter = new EventDetailsPresenter(new GetLocationForEventUseCase(
                    new LocationsRepository(new DatabaseHandler(getActivity())),
                    new EventsLocationsRepository(new DatabaseHandler(getActivity()))),
                    event);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());

        presenter.attachView(this);
        presenter.onCreated();

        getView().setBackgroundColor(Color.WHITE);
        getView().setClickable(true);

        initializeMapFragment();

        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_details, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        MenuItem item = menu.findItem(R.id.action_back_list);
        if(item != null)
            item.setVisible(false);
        inflater.inflate(R.menu.detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_back){

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(this);
            getFragmentManager().popBackStack();
            ft.commit();

            return true;
        }

        if(id == R.id.action_edit) {
            presenter.onEdit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setLocation(String location) {
        locationTextView.setText(location);
    }

    @Override
    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    @Override
    public void setDate(String date) {
        dateTextView.setText(date);
    }

    @Override
    public void setTime(String time) {
        timeTextView.setText(time);
    }

    @Override
    public void navigateToEditScreen(String serializedEvent) {
        Intent intent = new Intent(getActivity(), AddEventActivity.class);
        intent.putExtra(AddEventActivity.ARG_EVENT, serializedEvent);
        startActivityForResult(intent, RequestCode.EDIT_ACTIVITY);
    }

    @Override
    public void setDuration(int durationHours, int durationMinutes) {
        String durationText = getResources().getQuantityString(R.plurals.duration_hour, durationHours, durationHours)
                + " " +  getResources().getQuantityString(R.plurals.duration_minute, durationMinutes, durationMinutes);
        timeTextView.setText(durationText);
    }

    @Override
    public void onStop() {
        presenter.onStop();
        MenuItem item = menu.findItem(R.id.action_back_list);
        if(item != null)
            item.setVisible(true);
        if(showTabs) {
            ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
            ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        }
        if(oldTitle != null) {
            getActivity().setTitle(oldTitle);
        }
        super.onStop();
    }

    @Override
    public void onStart(){
        super.onStart();

        ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).findViewById(R.id.fab).setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

        getActivity().setTitle(event.getName());

        presenter.onStart();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.EDIT_ACTIVITY) {
            //remove tabs
            ((AppCompatActivity) getActivity()).findViewById(R.id.tab_layout).setVisibility(View.GONE);
            if (resultCode == Activity.RESULT_OK) {
                String serializedData = data.getStringExtra(AddEventActivity.ARG_EVENT);
                presenter.onEditedEvent(serializedData);
            }
        }
    }

    void initializeMapFragment(){
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.event_map, mapFragment);
        fragmentTransaction.commit();

        mapFragment.getMapAsync(this);
    }
}
