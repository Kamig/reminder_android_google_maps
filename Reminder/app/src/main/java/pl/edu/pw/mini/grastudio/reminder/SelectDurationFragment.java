package pl.edu.pw.mini.grastudio.reminder;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectDurationFragment extends Fragment {

    private static final String ARG_DURATION_HOUR = "durationHour";
    private static final String ARG_DURATION_MINUTE = "durationMinute";

    private int durationMinutes;
    private int durationHours;

    private final int MINUTE_MIN    =  0;
    private final int MINUTE_MAX    = 55;
    private final int MINUTE_STEP   =  5;

    @BindView(R.id.minutePicker)
    public NumberPicker minutePicker;
    @BindView(R.id.hourPicker)
    public NumberPicker hourPicker;


    public SelectDurationFragment() {
        // Required empty public constructor
    }


    public static SelectDurationFragment newInstance(int durationHours, int durationMinutes) {
        SelectDurationFragment fragment = new SelectDurationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DURATION_HOUR, durationHours);
        args.putInt(ARG_DURATION_MINUTE, durationMinutes);
        fragment.setArguments(args);
        //used to determine if between newInstance and onCreate someone set duration manually
        fragment.setDurationMinutes(-1);
        fragment.setDurationHours(-1);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(durationHours == -1) //only set if nobody set hours manually
                durationHours = getArguments().getInt(ARG_DURATION_HOUR);
            if(durationMinutes == -1) //only set if nobody set minutes manually
                durationMinutes = getArguments().getInt(ARG_DURATION_MINUTE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ButterKnife.bind(this, getView());

        String[] valueSet = new String[(MINUTE_MAX-MINUTE_MIN)/MINUTE_STEP+1];

        for (int i = MINUTE_MIN; i <= MINUTE_MAX; i += MINUTE_STEP) {
            valueSet[(i/MINUTE_STEP)] = String.valueOf(i);
        }

        minutePicker.setMinValue(MINUTE_MIN);
        minutePicker.setMaxValue(MINUTE_MAX / MINUTE_STEP);
        minutePicker.setDisplayedValues(valueSet);
        minutePicker.setValue(durationMinutes / MINUTE_STEP);

        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(23);
        hourPicker.setValue(durationHours);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_duration, container, false);
        return view;
    }

    public int getDurationHours() {
        if(hourPicker == null)
            return 0;
        return hourPicker.getValue();
    }

    public int getDurationMinutes() {
        if(minutePicker == null)
            return 0;
        return minutePicker.getValue() * MINUTE_STEP;
    }

    public void setDurationHours(int durationHours) {
        Log.i("ADD", String.valueOf(durationHours) + " hours");
        this.durationHours = durationHours;
        if(hourPicker != null) {
            hourPicker.setValue(durationHours);
        }
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
        if(minutePicker != null) {
            minutePicker.setValue(durationMinutes / MINUTE_STEP);
        }
    }
}
