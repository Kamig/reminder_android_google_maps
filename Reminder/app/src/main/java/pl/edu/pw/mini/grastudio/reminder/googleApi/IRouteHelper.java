package pl.edu.pw.mini.grastudio.reminder.googleApi;

import com.google.android.gms.maps.model.LatLng;

import pl.edu.pw.mini.grastudio.reminder.IMessage;
import rx.Observable;

public interface IRouteHelper {
    void TimeFromPointToPoint(LatLng origin, LatLng destination, int time, IMessage iMessage,int m);
    void RouteFromPointToPoint(LatLng origin, LatLng destination);
    int DistanceFromPointToPoint(LatLng currentPosition, LatLng destination);
    Observable<Integer> ObservableTimeFromPointToPoint(final LatLng origin, final LatLng destination, final int transportType);
}
