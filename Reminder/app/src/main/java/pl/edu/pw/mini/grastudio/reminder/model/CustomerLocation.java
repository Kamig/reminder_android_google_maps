package pl.edu.pw.mini.grastudio.reminder.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Kamil on 15.04.2017.
 */

public class CustomerLocation extends Entity {

    private String name;
    private double lat;
    private double lng;
    private boolean favorite;

    public CustomerLocation(long id, String name, double lat, double lng) {
        super(id);
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.favorite = false;

    }

    public CustomerLocation(long id, String name, double lat, double lng, boolean favorite) {
        super(id);
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
