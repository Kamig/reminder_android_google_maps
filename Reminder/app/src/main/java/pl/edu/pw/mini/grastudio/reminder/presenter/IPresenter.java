package pl.edu.pw.mini.grastudio.reminder.presenter;


import pl.edu.pw.mini.grastudio.reminder.view.View;

public interface IPresenter<T extends View> {
    void onCreate();

    void onStart();

    void onStop();

    void onPause();

    void attachView(T view);
}
