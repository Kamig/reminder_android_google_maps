package pl.edu.pw.mini.grastudio.reminder.presenter;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.domain.IUseCase;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.view.ISelectLocationView;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class SelectLocationPresenter implements ISelectLocationPresenter {

    ISelectLocationView selectLocationView;
    IUseCase<List<CustomerLocation>> getFavoriteLocationsUseCase;
    Subscription getFavoriteLocationSubscriptions;

    public SelectLocationPresenter(IUseCase<List<CustomerLocation>> getFavoriteLocationsUseCase) {
        this.getFavoriteLocationsUseCase = getFavoriteLocationsUseCase;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (getFavoriteLocationSubscriptions != null && !getFavoriteLocationSubscriptions.isUnsubscribed()) {
            getFavoriteLocationSubscriptions.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(ISelectLocationView view) {
        this.selectLocationView = view;

        getFavoriteLocationSubscriptions = getFavoriteLocationsUseCase.execute()
                .observeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, List<CustomerLocation>>() {
                    @Override
                    public List<CustomerLocation> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return null;
                    }
                })
                .subscribe(new Action1<List<CustomerLocation>>() {
                    @Override
                    public void call(List<CustomerLocation> customerLocations) {
                        selectLocationView.setFavoriteList(customerLocations);
                    }
                });
    }
}
