package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.IDateService;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Kamil on 13.05.2017.
 */

public class GetTheNearestEventUseCase implements IUseCase<Event> {


    private IRepository<Event> eventRepository;
    private IDateService dateService;

    public GetTheNearestEventUseCase(IRepository<Event> eventRepository, IDateService dateService)
    {
        this.eventRepository = eventRepository;
        this.dateService = dateService;
    }

    @Override
    public Observable<Event> execute() {

        return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(Event event) {
                return event.getStartDate().after(dateService.Now())
                        && !event.isAllDay();
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        }).flatMap(new Func1<List<Event>, Observable<Event>>() {
            @Override
            public Observable<Event> call(List<Event> events) {
                return Observable.just(events.get(0));

            }
        });

    }
}
