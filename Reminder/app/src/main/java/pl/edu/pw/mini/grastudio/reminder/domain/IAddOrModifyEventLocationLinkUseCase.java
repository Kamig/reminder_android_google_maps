package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;

public interface IAddOrModifyEventLocationLinkUseCase extends IUseCase<Long> {
    void setLinkToAdd(EventLocationLink linkToAdd);
}
