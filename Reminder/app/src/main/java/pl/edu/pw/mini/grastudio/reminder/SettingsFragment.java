package pl.edu.pw.mini.grastudio.reminder;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;

import android.preference.PreferenceFragment;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 25.05.2017.
 */

public class SettingsFragment extends PreferenceFragment {

    //@BindView(R.id.closeButton)
    //public Button closeButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
      addPreferencesFromResource(R.layout.preferences);

       // ButterKnife.bind(getActivity());



    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(Color.WHITE);


//        closeButton = (Button) view.findViewById(R.id.closeButton);
//
//        closeButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), TabActivity.class);
//                startActivity(intent);
//            }
//        });
        return view;
    }


}

