package pl.edu.pw.mini.grastudio.reminder.presenter;


import com.google.android.gms.maps.model.LatLng;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventDetailsView;

public interface IEventDetailsPresenter extends IPresenter<IEventDetailsView> {
    void setEvent(Event event);
    void onCreated();
    void onEdit();
    void onEditedEvent(String serializedData);
    LatLng getLatLngLocation();
}
