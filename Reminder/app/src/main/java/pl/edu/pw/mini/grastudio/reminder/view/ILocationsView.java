package pl.edu.pw.mini.grastudio.reminder.view;

import android.location.Location;

import java.util.HashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;

/**
 * Created by Kamil on 14.04.2017.
 */

public interface ILocationsView extends View {
    void initializeMap();
    void updateCurrentLocation();
    void initializeMarkerClickListener();
     void initializeMapClickListener();
    void getLocations(List<CustomerLocation> locations);
    void initializeLocationRequest();
    void locationPermissionEnable(int requestCode, String permissions[], int[] grantResults);
    void startPlacePickerDialog();
    void addMarker(long id, String name, double lat, double lng);
    void displayEventDetailsFragment(Event event);
    void displayTimeFilterFragment();
    void toastMessage(String message);
    void deleteMarkers();
    void displayEventListFragment(List<Event> events);
}
