package pl.edu.pw.mini.grastudio.reminder.presenter;


import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventListView;

public interface IEventListPresenter extends IPresenter<IEventListView> {
    void setEvents(List<Event> events);

    void onMenuBack();
}
