package pl.edu.pw.mini.grastudio.reminder.domain;

import android.util.Log;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.IDateService;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Kamil on 11.05.2017.
 */

public class GetLocationForTheNearestEventUseCase implements IUseCase<CustomerLocation> {

    private IRepository<EventLocationLink> eventsLocationsRepository;
    private IRepository<CustomerLocation> locationsRepository;
    private IRepository<Event> eventRepository;
    private IDateService dateService;

    public GetLocationForTheNearestEventUseCase (IRepository<Event> eventRepository, IRepository<CustomerLocation> locationsRepository, IRepository<EventLocationLink> eventsLocationsRepository, IDateService dateService)
    {
        this.locationsRepository = locationsRepository;
        this.eventsLocationsRepository = eventsLocationsRepository;
        this.eventRepository = eventRepository;
        this.dateService = dateService;
    }

    @Override
    public Observable<CustomerLocation> execute() {

           return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(Event event) {
                return event.getStartDate().after(dateService.Now())
                         && !event.isAllDay();
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        }).flatMap(new Func1<List<Event>, Observable<CustomerLocation>>() {
                    @Override
                    public Observable<CustomerLocation> call(List<Event> events) {
                        return ((IEventsLocationsRepository)eventsLocationsRepository).getByEventId(events.get(0).getId()).flatMap(new Func1<EventLocationLink, Observable<CustomerLocation>>() {
                            @Override
                            public Observable<CustomerLocation> call(EventLocationLink eventLocationLink) {
                                //Log.i("MAIN", "Loc. id is: " + Long.toString(eventLocationLink.getLocationId()));
                                return locationsRepository.getById(eventLocationLink.getLocationId());
                            }
                        });
                    }
                });

    }
}