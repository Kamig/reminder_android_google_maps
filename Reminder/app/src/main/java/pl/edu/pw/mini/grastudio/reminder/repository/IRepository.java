package pl.edu.pw.mini.grastudio.reminder.repository;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Entity;
import rx.Observable;

public interface IRepository<T extends Entity> {
    Observable<T> getById(long id);
    Observable<List<T>> getAll();
    //returns id of inserted/modified element
    Observable<Long> addOrUpdate(T item);
    Observable<Integer> delete(long id);
}
