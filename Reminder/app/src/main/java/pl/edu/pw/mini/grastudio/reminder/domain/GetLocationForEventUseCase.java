package pl.edu.pw.mini.grastudio.reminder.domain;

import android.util.Log;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;

public class GetLocationForEventUseCase implements IGetLocationForEventUseCase {

    private Event event;
    private IEventsLocationsRepository eventsLocationsRepository;
    private IRepository<CustomerLocation> locationsRepository;

    public GetLocationForEventUseCase(IRepository<CustomerLocation> locationsRepository, IEventsLocationsRepository eventsLocationsRepository)
    {
        this.locationsRepository = locationsRepository;
        this.eventsLocationsRepository = eventsLocationsRepository;
    }

    @Override
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public Observable<CustomerLocation> execute() {

        return eventsLocationsRepository.getByEventId(event.getId()).flatMap(new Func1<EventLocationLink, Observable<CustomerLocation>>() {
            @Override
            public Observable<CustomerLocation> call(EventLocationLink eventLocationLink) {
//                Log.i("MAIN", "Loc. id is: " + Long.toString(eventLocationLink.getLocationId()));
                return locationsRepository.getById(eventLocationLink.getLocationId());
            }
        });
    }
}
