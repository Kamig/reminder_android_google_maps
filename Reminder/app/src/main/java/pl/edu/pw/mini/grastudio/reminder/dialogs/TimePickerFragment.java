package pl.edu.pw.mini.grastudio.reminder.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private static final String INITIAL_HOUR = "INITIAL_HOUR";
    private static final String INITIAL_MINUTE = "INITIAL_MINUTE";

    OnTimePickListener listener;

    public static TimePickerFragment newInstance(int hour, int minute, OnTimePickListener listener) {

        Bundle args = new Bundle();
        args.putInt(INITIAL_HOUR, hour);
        args.putInt(INITIAL_MINUTE, minute);
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        fragment.listener = listener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int hour = getArguments().getInt(INITIAL_HOUR);
        int minute = getArguments().getInt(INITIAL_MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(listener != null) {
            listener.OnTimePick(hourOfDay, minute);
        }
    }

    public interface OnTimePickListener {
        void OnTimePick(int hour, int minute);
    }
}
