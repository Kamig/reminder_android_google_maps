package pl.edu.pw.mini.grastudio.reminder.domain;

import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;

/**
 * Created by Kamil on 28.04.2017.
 */

public class GetLocationByIdUseCase implements IGetLocationByIdUseCase {
    private IRepository<CustomerLocation> locationRepository;

    private long id;

    public GetLocationByIdUseCase(IRepository<CustomerLocation> locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }


    @Override
    public Observable<CustomerLocation> execute() {
        return locationRepository.getById(id);
    }


}
