package pl.edu.pw.mini.grastudio.reminder.domain;

import android.text.format.DateUtils;
import android.util.Log;

import java.text.Collator;
import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Kamil on 03.05.2017.
 */

public class GetFavoriteLocationsUseCase implements IUseCase<List<CustomerLocation>> {
    private IRepository<CustomerLocation> locationRepository;

    public GetFavoriteLocationsUseCase(IRepository<CustomerLocation> locationRepository) {
        this.locationRepository = locationRepository;
    }
    @Override
    public Observable<List<CustomerLocation>> execute() {
        //we need to use flatMapIterable, because otherwise we cannot filter the list
        return locationRepository.getAll().flatMapIterable(new Func1<List<CustomerLocation>, List<CustomerLocation>>() {
            @Override
            public List<CustomerLocation> call(List<CustomerLocation> locations) {
                return locations;
            }
        }).filter(new Func1<CustomerLocation, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(CustomerLocation customerLocation) {
                return customerLocation.isFavorite();
            }
        }).toSortedList(new Func2<CustomerLocation, CustomerLocation, Integer>() {
            @Override
            public Integer call(CustomerLocation location, CustomerLocation location2) {
                //sort according to locale!
                Collator myDefaultCollator = Collator.getInstance();
                return myDefaultCollator.compare(location.getName().toLowerCase(), location2.getName().toLowerCase());
            }
        });
    }
}