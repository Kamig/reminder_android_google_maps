package pl.edu.pw.mini.grastudio.reminder.domain;


import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;

public class GetEventForLocationUseCase implements IGetEventForLocationUseCase {

    private CustomerLocation location;
    private IEventsLocationsRepository eventsLocationsRepository;
    private IRepository<Event> eventRepository;

    public GetEventForLocationUseCase(IEventsLocationsRepository eventsLocationsRepository, IRepository<Event> eventRepository, CustomerLocation location) {
        this.location = location;
        this.eventsLocationsRepository = eventsLocationsRepository;
        this.eventRepository = eventRepository;
    }

    @Override
    public void setLocation(CustomerLocation location) {
        this.location = location;
    }

    @Override
    public void setId(long id) {
        this.location = new CustomerLocation(id, "", 0, 0);
    }

    @Override
    public Observable<List<Event>> execute() {
        return eventsLocationsRepository.getByLocationId(location.getId()).flatMapIterable(new Func1<List<EventLocationLink>, Iterable<EventLocationLink>>() {
            @Override
            public Iterable<EventLocationLink> call(List<EventLocationLink> eventLocationLinks) {
                return eventLocationLinks;
            }
        }).flatMap(new Func1<EventLocationLink, Observable<Event>>() {
            @Override
            public Observable<Event> call(EventLocationLink eventLocationLink) {
                return eventRepository.getById(eventLocationLink.getEventId());
            }
        }).toList();
    }
}
