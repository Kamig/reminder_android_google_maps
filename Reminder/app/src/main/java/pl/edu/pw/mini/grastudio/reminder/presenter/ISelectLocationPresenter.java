package pl.edu.pw.mini.grastudio.reminder.presenter;

import pl.edu.pw.mini.grastudio.reminder.view.ISelectLocationView;

public interface ISelectLocationPresenter extends IPresenter<ISelectLocationView> {
}
