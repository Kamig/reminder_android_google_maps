package pl.edu.pw.mini.grastudio.reminder.view;

public interface IEventDetailsView extends View {
    void setLocation(String location);
    void setTitle(String title);
    void setDate(String date);
    void setTime(String time);
    void navigateToEditScreen(String serializedEvent);
    void setDuration(int durationHours, int durationMinutes);
}
