package pl.edu.pw.mini.grastudio.reminder.domain;

import android.util.Log;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;
import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationContener;
import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Kamil on 16.05.2017.
 */

public class GetContenerForEventUseCase  implements IGetContenerForEventUseCase {

    private Event event;
    private IEventsLocationsRepository eventsLocationsRepository;
    private IRepository<CustomerLocation> locationsRepository;

    public GetContenerForEventUseCase(IRepository<CustomerLocation> locationsRepository, IEventsLocationsRepository eventsLocationsRepository)
    {
        this.locationsRepository = locationsRepository;
        this.eventsLocationsRepository = eventsLocationsRepository;
    }

    @Override
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public Observable<EventLocationContener> execute() {

        return eventsLocationsRepository.getByEventId(event.getId()).flatMap(new Func1<EventLocationLink, Observable<EventLocationContener>>() {
            @Override
            public Observable<EventLocationContener> call(EventLocationLink eventLocationLink) {
                Log.i("MAIN", "Loc. id is: " + Long.toString(eventLocationLink.getLocationId()));
                return locationsRepository.getById(eventLocationLink.getLocationId()).flatMap(new Func1<CustomerLocation, Observable<EventLocationContener>>() {
                    @Override
                    public Observable<EventLocationContener> call(CustomerLocation customerLocation) {
                       return Observable.just(new EventLocationContener(event, customerLocation));
                    }
                });
            }
        });
    }
}