package pl.edu.pw.mini.grastudio.reminder.domain;

import android.text.format.DateUtils;

import java.util.Date;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import pl.edu.pw.mini.grastudio.reminder.utils.IDateService;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by HP on 2017-05-17.
 */

public class GetFilteredEventsUseCase implements IUseCase<List<Event>> {
    private IRepository<Event> eventRepository;

    Date startDate;
    Date endDate;

    public GetFilteredEventsUseCase(IRepository<Event> eventRepository) {
        this.eventRepository = eventRepository;
    }
    public void setStartDate(Date date){
        startDate = date;
    }
    public void setEndDate(Date date){
        endDate = date;
    }
    @Override
    public Observable<List<Event>> execute() {
        //we need to use flatMapIterable, because otherwise we cannot filter the list
        return eventRepository.getAll().flatMapIterable(new Func1<List<Event>, List<Event>>() {
            @Override
            public List<Event> call(List<Event> events) {
                return events;
            }
        }).filter(new Func1<Event, Boolean>() {
            //show only events after current date or events that last all day and are today
            @Override
            public Boolean call(Event event) {
                return  event.getStartDate().after(startDate) && event.getEndDate().before(endDate);
            }
        }).toSortedList(new Func2<Event, Event, Integer>() { //sort chronologically
            @Override
            public Integer call(Event event, Event event2) {
                return  event.getStartDate().compareTo(event2.getStartDate());
            }
        });
    }
}
