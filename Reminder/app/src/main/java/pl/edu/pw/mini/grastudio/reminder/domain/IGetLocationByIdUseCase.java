package pl.edu.pw.mini.grastudio.reminder.domain;

import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

public interface IGetLocationByIdUseCase extends IUseCase<CustomerLocation> {
    void setId(long id);
}
