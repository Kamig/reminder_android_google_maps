package pl.edu.pw.mini.grastudio.reminder.view;


import java.util.Date;

public interface IAddEventView extends View {
    void loadDateAndTime(String date, String time);

    void showStartDateDialog(int year, int month, int dayOfMonth);
    void setStartDate(String date);
    void showEndDateDialog(int year, int month, int dayOfMonth);
    void setEndDate(String date);

    void showStartTimeDialog(int hours, int minutes);
    void showEndTimeDialog(int hours, int minutes);
    void setStartTime(String time);
    void setEndTime(String time);

    void returnToLastScreen();
    void returnToLastScreenWithResult(String serializedResult);

    void startPlacePickerDialog();
    void setLocationText(String s);

    void showDurationFragment(int hours, int minutes);
    void showTimeFragment(String startTime, String endTime);
    void reloadDurationFragment();
    void reloadTimeFragment();

    void setDuration(int durationHours, int durationMinutes);
    void setTitle(String title);
    void setAllDay(boolean allDay);
    void setSelectedPriority(int index);
    void setTransport(int transportInt);
    void setDistance(double reminderDistanceMeters);
}
