package pl.edu.pw.mini.grastudio.reminder;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pw.mini.grastudio.reminder.googleApi.LocationHelper;
import pl.edu.pw.mini.grastudio.reminder.googleApi.RouteHelper;
import pl.edu.pw.mini.grastudio.reminder.model.CustomerLocation;

import static pl.edu.pw.mini.grastudio.reminder.MapsActivity.MY_PERMISSIONS_REQUEST_LOCATION;

public class RouteActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private RouteHelper routeHelper;
    private LocationHelper locationHelper;
    private Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Marker mCurrLocationMarker;
    private CustomerLocation mEventLocation;
    private int travelingModeChoice;

    private TabLayout tabLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


//        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
//        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.map, mapFragment);
//        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
        routeHelper = new RouteHelper(this);
        checkExtras();
        initializeTabLayout();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //locationHelper =  new LocationHelper(this);

        initializeMap();
        //initializeMapClickListener();

    }
//    RouteActivity route = this;
//    public void initializeMapClickListener(){
////        final RouteHelper routeHelper = new RouteHelper(this);
//        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
//                LatLng latLng1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//                routeHelper.RouteFromPointToPoint(latLng, latLng1, route);
//            }
//        });
//    }

//    @Override
//    public void ShowMessage() {
//        Toast.makeText(this,"MESSAGE", Toast.LENGTH_LONG );
//    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        initializeLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("INFO",String.valueOf(location.getLatitude()));
        mLastLocation = location;
        updateCurrentLocation();
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    public void initializeLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    public void updateCurrentLocation() {
        if(mCurrLocationMarker != null){
            mCurrLocationMarker.remove();
        }

        addMarker();

        LatLng latLng1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        LatLng latLng2 = new LatLng(mEventLocation.getLat(), mEventLocation.getLng());
        routeHelper.RouteFromPointToPoint(latLng1, latLng2, this, travelingModeChoice);





        //stop location updates
        if(mGoogleApiClient != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        setTabText();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        locationPermissionEnable(requestCode, permissions, grantResults);
    }


    public void locationPermissionEnable(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_LOCATION:{
                //If request is cancelled, the result arrays are empty.
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Permission was granted.
                    if(ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                        if(mGoogleApiClient == null){
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }else {
                    //Pemission denied, Disable the functionality that depends on this permission.
                    //toastMessage("permission denied");
                }
                return;
            }
            //other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }
    }

    public void initializeMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google play services
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED){
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    public void drawRoutes(List<PolylineOptions> polylineOptionsList){
        for(PolylineOptions line : polylineOptionsList){
            mMap.addPolyline(line);
        }
    }

    private void checkExtras(){
        Intent intent = getIntent();
        String title = intent.getStringExtra(getResources().getString(R.string.extra_title));
        double lat = intent.getDoubleExtra(getResources().getString(R.string.extra_lat), 0);
        double lng = intent.getDoubleExtra(getResources().getString(R.string.extra_lng), 0);
        travelingModeChoice = intent.getIntExtra(getResources().getString(R.string.traveling_mode), 0);
        mEventLocation = new CustomerLocation(-1, title, lat, lng);



    }

    private void initializeTabLayout(){
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        int mode = travelingModeChoice;
        TabLayout.Tab tab = tabLayout.getTabAt(mode);
        tab.select();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
              travelingModeChoice = tab.getPosition();
                LatLng latLng1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                LatLng latLng2 = new LatLng(mEventLocation.getLat(), mEventLocation.getLng());
                mMap.clear();
                addMarker();
                routeHelper.RouteFromPointToPoint(latLng1, latLng2, RouteActivity.this, travelingModeChoice);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void addMarker(){

        //Place current location marker
        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(yourLocation);

        //Place event location marker
         latLng = new LatLng(mEventLocation.getLat(), mEventLocation.getLng());
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMap.addMarker(markerOptions);


    }

    private void setTabText(){
        for(int i = 0; i <= 3; i++){
            LatLng latLng1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng latLng2 = new LatLng(mEventLocation.getLat(), mEventLocation.getLng());
            routeHelper.TimeFromPointToPoint(latLng1, latLng2, tabLayout.getTabAt(i), i);
        }
    }


}
