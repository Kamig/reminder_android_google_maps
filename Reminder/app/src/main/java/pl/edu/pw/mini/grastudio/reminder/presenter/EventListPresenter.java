package pl.edu.pw.mini.grastudio.reminder.presenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.view.IEventListView;

public class EventListPresenter implements IEventListPresenter {

    private IEventListView view;

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(IEventListView view) {
        this.view = view;
    }

    @Override
    public void setEvents(List<Event> events) {
        view.setGroupedEvents(groupEventsByDay(events));
    }

    @Override
    public void onMenuBack() {
        view.returnToLastScreen();
    }

    private HashMap<String, List<Event>> groupEventsByDay(List<Event> events) {
        //we use a LinkedHashMap to preserve insertion order
        HashMap<String, List<Event>> result = new LinkedHashMap<>();
        SimpleDateFormat withoutTimeDateFormat = new SimpleDateFormat("yyyy.MM.dd");

        for(Event e : events) {
            String key = withoutTimeDateFormat.format(e.getStartDate());
            if(result.get(key) == null) { //event on a new, unused day
                result.put(key, new ArrayList<Event>());
            }
            result.get(key).add(e);
        }

        return  result;
    }
}
