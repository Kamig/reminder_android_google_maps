package pl.edu.pw.mini.grastudio.reminder;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.EventLocationLink;
import pl.edu.pw.mini.grastudio.reminder.repository.IEventsLocationsRepository;
import rx.Observable;


public class MockEventLocationsRepository implements IEventsLocationsRepository {

    List<EventLocationLink> eventLocationLinks = new ArrayList<>();
    long lastId = 0;
    @Override
    public Observable<EventLocationLink> getById(long id) {
        for(int i = 0; i < eventLocationLinks.size(); i++)
            if(eventLocationLinks.get(i).getId() == id)
                return Observable.just(eventLocationLinks.get(i));
        return null;
    }

    @Override
    public Observable<List<EventLocationLink>> getAll() {
        return Observable.just(eventLocationLinks);
    }

    @Override
    public Observable<Long> addOrUpdate(EventLocationLink item) {
        //only adding
        long oldId = lastId++;
        item.setId(oldId);
        eventLocationLinks.add(item);
        return Observable.just(oldId);
    }

    @Override
    public Observable<Integer> delete(long id) {
        return null;
    }

    @Override
    public Observable<EventLocationLink> getByEventId(long id) {
        for(int i = 0; i < eventLocationLinks.size(); i++)
            if(eventLocationLinks.get(i).getEventId() == id)
                return Observable.just(eventLocationLinks.get(i));
        return null;
    }

    @Override
    public Observable<List<EventLocationLink>> getByLocationId(long id) {
        List<EventLocationLink> result = new ArrayList<>();
        for(int i = 0; i < eventLocationLinks.size(); i++)
            if(eventLocationLinks.get(i).getLocationId() == id)
                result.add(eventLocationLinks.get(i));
        return Observable.just(result);
    }
}
