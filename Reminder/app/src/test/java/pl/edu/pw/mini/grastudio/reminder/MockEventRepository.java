package pl.edu.pw.mini.grastudio.reminder;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pw.mini.grastudio.reminder.model.Event;
import pl.edu.pw.mini.grastudio.reminder.repository.IRepository;
import rx.Observable;


public class MockEventRepository implements IRepository<Event> {
    List<Event> events = new ArrayList<>();
    long lastId = 0;

    @Override
    public Observable<Event> getById(long id) {
        for(int i = 0; i < events.size(); i++)
            if(events.get(i).getId() == id)
                return Observable.just(events.get(i));
        return null;
    }

    @Override
    public Observable<List<Event>> getAll() {
        return Observable.just(events);
    }

    @Override
    public Observable<Long> addOrUpdate(Event item) {
        //only adding
        long oldId = lastId++;
        item.setId(oldId);
        events.add(item);
        return Observable.just(oldId);
    }

    @Override
    public Observable<Integer> delete(long id) {
        return null;
    }
}
